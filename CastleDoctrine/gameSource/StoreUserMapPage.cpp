#include "StoreUserMapPage.h"
#include "ticketHash.h"
#include "message.h"

#include "serialWebRequests.h"

#include "minorGems/game/Font.h"
#include "minorGems/game/game.h"

#include "minorGems/util/stringUtils.h"
#include "minorGems/network/web/URLUtils.h"


extern Font *mainFont;

extern char *serverURL;

extern int userID;

StoreUserMapPage::StoreUserMapPage()
        : mWebRequest( -1 ),
          mHomeButton( mainFont, 4, -4, translate( "returnHome" ) ),
          mReturnToHome( false ),
          mHouseCost( 0 ),
          mIsStoringMode( false ),
          mSuccess( false ),
          mMapNumber( 0 )
{

    addComponent( &mHomeButton );
    mHomeButton.addActionListener( this );
    mHomeButton.setMouseOverTip( "" );
}

        
StoreUserMapPage::~StoreUserMapPage()
{
    if( mWebRequest != -1 )
    {
        clearWebRequestSerial( mWebRequest );
    }
}


char StoreUserMapPage::getReturnToHome()
{
    return mReturnToHome;
}


void StoreUserMapPage::setIsStoringMode( char isStoringMode )
{
    mIsStoringMode = isStoringMode;
}


char StoreUserMapPage::getIsStoringMode()
{
    return mIsStoringMode;
}


void StoreUserMapPage::actionPerformed( GUIComponent *inTarget )
{
    if( inTarget == &mHomeButton )
    {
        mReturnToHome = true;
    }
}


void StoreUserMapPage::step()
{
    if( mWebRequest != -1 )
    {
        int stepResult = stepWebRequestSerial( mWebRequest );
        
        if( stepResult != 0 )
        {
            setWaiting( false );
            mHomeButton.setVisible( true );
            
            if( mIsStoringMode )
            {
                blockQuitting( false );
            }
        }
        
        switch( stepResult )
        {
            case 0:
                break;
                
            case -1:
                mStatusError = true;
                mStatusMessageKey = "err_webRequest";
                clearWebRequestSerial( mWebRequest );
                mWebRequest = -1;
                break;
                
            case 1: {
                char *result = getWebResultSerial( mWebRequest );
                clearWebRequestSerial( mWebRequest );
                mWebRequest = -1;
                     
                printf( "Web result = %s\n", result );
                
                if( strstr( result, "DENIED" ) != NULL )
                {
                    mStatusError = true;
                    
                    if( mIsStoringMode )
                    {
                        mStatusMessageKey = "storeUserMapFailed";
                    }
                    else
                    {
                        mStatusMessageKey = "restoreUserMapFailed";
                    }
                }
                else if( strstr( result, "NOT_ENOUGH_CASH" ) != NULL )
                {
                    mStatusError = true;
                    mStatusMessageKey = "restoreUserMapFailedCost";
                    
                    SimpleVector<char *> *tokens = tokenizeString( result );
                    if( tokens->size() == 5 )
                    {
                        sscanf( *( tokens->getElement( 0 ) ), "%d", &mHouseCost );
                        sscanf( *( tokens->getElement( 1 ) ), "%d", &mCostSaved );
                        sscanf( *( tokens->getElement( 2 ) ), "%d", &mTotalCost );
                        sscanf( *( tokens->getElement( 3 ) ), "%d", &mMissingCash );
                    }
                    else
                    {
                        mStatusMessageKey = "err_badServerResponse";
                    }
                }
                else if( strstr( result, "HOUSE_BEING_ROBBED" ) != NULL )
                {
                    mStatusError = true;
                    mStatusMessageKey = "restoreUserMapFailedRobbery";
                }
                else if( strstr( result, "EDIT_COUNT_BELOW_ZERO" ) != NULL )
                {
                    mStatusError = true;
                    if( mIsStoringMode )
                    {
                        mStatusMessageKey = "storeUserMapFailedEditCount";
                    }
                    else
                    {
                        mStatusMessageKey = "restoreUserMapFailedEditCount";
                    }
                }
                else if( strstr( result, "MISSING_MAP" ) != NULL )
                {
                    mStatusError = true;
                    mStatusMessageKey = "restoreUserMapFailedMapMissing";
                }
                else if( strstr( result, "FAMILY_PLACEMENT_FAILED" ) != NULL )
                {
                    mStatusError = true;
                    mStatusMessageKey = "restoreUserMapFailedFamily";
                }
                else
                {
                    SimpleVector<char *> *tokens = tokenizeString( result );
                    
                    if( mIsStoringMode && tokens->size() == 2 &&
                        strcmp( *( tokens->getElement( 1 ) ), "OK" ) == 0 )
                    {
                        mSuccess = true;
                        sscanf( *( tokens->getElement( 0 ) ), "%d", &mHouseCost );
                    }
                    else if( !mIsStoringMode && tokens->size() == 4 &&
                             strcmp( *( tokens->getElement( 3 ) ), "OK" ) == 0 )
                    {
                        mSuccess = true;
                        // read the house cost from the resonse
                        sscanf( *( tokens->getElement( 0 ) ), "%d", &mHouseCost );
                        sscanf( *( tokens->getElement( 1 ) ), "%d", &mCostSaved );
                        sscanf( *( tokens->getElement( 2 ) ), "%d", &mTotalCost );
                    }
                    else
                    {
                        mStatusError = true;
                        mStatusMessageKey = "err_badServerResponse";
                    }
                }
                break;
            }
        }
    }
}

        
void StoreUserMapPage::makeActive( char inFresh )
{
    if( !inFresh )
    {
        return;
    }
    
    char *ticketHash = getTicketHash();
    
    char *action;
    
    if( mIsStoringMode )
    {
        action = autoSprintf( "store_user_map" );
        mHomeButton.setLabelText( translate( "doneEdit" ) );
    }
    else
    {
        action = autoSprintf( "restore_user_map" );
        mHomeButton.setLabelText( translate( "returnHome" ) );
    }
    
    char *actionString = autoSprintf(
        "action=%s"
        "&user_id=%d"
        "&map_number=%d"
        "&%s",
        action, userID, mMapNumber, ticketHash );
    
    delete[] ticketHash;
    
    // send the web request
    mWebRequest = startWebRequestSerial( "POST", 
                                   serverURL, 
                                   actionString );

    delete[] actionString;

    // reset all data
    mReturnToHome = false;
    mHouseCost = 0;
    mCostSaved = 0;
    mTotalCost = 0;
    mMissingCash = 0;
    
    mSuccess = false;
    
    mStatusError = false;
    mStatusMessageKey = NULL;

    setStatusDirect( NULL, false );

    mHomeButton.setVisible( false );

    setWaiting( true );
}


void StoreUserMapPage::draw( doublePair inViewCenter,
                                double inViewSize )
{
    double Y_START = 4.0;

    if( ! mHomeButton.isVisible() )
    {
        return;
    }
    
    doublePair labelPos = { 0, Y_START };

    if( mSuccess )
    {
        if( mIsStoringMode )
        {
            drawMessage( translate( "storeUserMapSuccess" ) , labelPos, false );
        }
        else
        {
            drawMessage( translate( "restoreUserMapSuccess" ) , labelPos, false );
        }
    }
    else
    {
        if( mIsStoringMode )
        {
            drawMessage( translate( "storeUserMapFailed" ) , labelPos, false );
        }
        else
        {
            drawMessage( translate( "restoreUserMapFailed" ) , labelPos, false );
        }
    }
    
    // print cost information for map
    labelPos.y--;
    if( mHouseCost >= 0 )
    {
        char *costString = autoSprintf( translate( "userMapHouseCost" ),
                                        mHouseCost );

        drawMessage( costString, labelPos, false );
        delete[] costString;
    }

    if( !mIsStoringMode && mCostSaved >= 0 )
    {
        char *costSavedString = autoSprintf( translate( "userMapHouseCostSaved" ),
                                             mCostSaved );
        labelPos.y--;
        drawMessage( costSavedString, labelPos, false );
        delete[] costSavedString;
    }
    
    if( !mIsStoringMode && mTotalCost >= 0 )
    {
    
        char *rulerString = autoSprintf( translate( "userMapHouseRuler" ) );
        char *totalCostString = autoSprintf( translate( "userMapHouseTotalCost" ),
                                             mTotalCost );
        labelPos.y--;
        drawMessage( rulerString, labelPos, false );
        labelPos.y--;
        drawMessage( totalCostString, labelPos, false );
        
        delete[] totalCostString;
        delete[] rulerString;
    }
    
    if( !mIsStoringMode && mMissingCash > 0 )
    {
        char *missingCashString = autoSprintf( translate( "userMapHouseMissingCash" ),
                                               mMissingCash );
        labelPos.y--;
        drawMessage( missingCashString, labelPos, false );
        delete[] missingCashString;
    }
    
}


void StoreUserMapPage::setMapNumber( int mapNumber )
{
    mMapNumber = mapNumber;
}

