#include "StatisticsPage.h"
#include "ticketHash.h"
#include "message.h"

#include "serialWebRequests.h"

#include "minorGems/game/Font.h"
#include "minorGems/game/game.h"

#include "minorGems/util/stringUtils.h"
#include "minorGems/network/web/URLUtils.h"


extern Font *mainFont;

extern char *serverURL;

extern int userID;

// this is the draw order of statistics data
// objects with id 0, 1, 2 and 3 are ommitted
// (user_id, unique_users, database_connections, max_concurrent_connections)
// because they are not relevant for a single user
const int StatisticsPage::statisticsOrder[ StatisticsPage::NUM_STATISTIC_ATTRIBUTES ] =
            { 13, 15, 14, 18, 19, 20, 23, 30,
              5,  6,  7,  -1, 8,  9,  10, 32,
              16, 17, 31, -1, 4,  38, 39, -1,
              26, 24, 21, 11, 22, -1, 25, 12,
              27, 28, 29, 36, 33, 34, 37, 35 };

const int StatisticsPage::isAMoneyStatistic[ StatisticsPage::NUM_STATISTIC_ATTRIBUTES ] =
            { 0, 0, 0, 0, 0, 0, 1, 1,
              0, 0, 0, 0, 1, 1, 1, 1,
              0, 0, 1, 0, 0, 0, 0, 0,
              0, 0, 0, 0, 0, 0, 1, 1,
              0, 0, 0, 0, 0, 0, 0, 0 };


StatisticsPage::StatisticsPage()
        : mWebRequest( -1 ),
          mMenuButton( mainFont, 8, -6, translate( "returnMenu" ) ),
          mReturnToMenu( false ),
          mCategorie1Button( mainFont, -8, 6, translate( "statisticsCategorie1" ) ),
          mCategorie2Button( mainFont, -4, 6, translate( "statisticsCategorie2" ) ),
          mCategorie3Button( mainFont, 0, 6, translate( "statisticsCategorie3" ) ),
          mCategorie4Button( mainFont, 4, 6, translate( "statisticsCategorie4" ) ),
          mCategorie5Button( mainFont, 8, 6, translate( "statisticsCategorie5" ) ),
          mCategorieChanged( false ),
          mStatisticAttributesOffset( 1 ),
          mCurrentHighlightedButton( NULL )
{

    addComponent( &mMenuButton );
    addComponent( &mCategorie1Button );
    addComponent( &mCategorie2Button );
    addComponent( &mCategorie3Button );
    addComponent( &mCategorie4Button );
    addComponent( &mCategorie5Button );
    mMenuButton.addActionListener( this );
    mCategorie1Button.addActionListener( this );
    mCategorie2Button.addActionListener( this );
    mCategorie3Button.addActionListener( this );
    mCategorie4Button.addActionListener( this );
    mCategorie5Button.addActionListener( this );
    mMenuButton.setMouseOverTip( "" );
    mCategorie1Button.setMouseOverTip( translate( "statisticsCategorie1" ) );
    mCategorie2Button.setMouseOverTip( translate( "statisticsCategorie2" ) );
    mCategorie3Button.setMouseOverTip( translate( "statisticsCategorie3" ) );
    mCategorie4Button.setMouseOverTip( translate( "statisticsCategorie4" ) );
    mCategorie5Button.setMouseOverTip( translate( "statisticsCategorie5" ) );
    
    mCategorie1Button.setVisible( false );
    mCategorie2Button.setVisible( false );
    mCategorie3Button.setVisible( false );
    mCategorie4Button.setVisible( false );
    mCategorie5Button.setVisible( false );
}

        
StatisticsPage::~StatisticsPage()
{
    if( mWebRequest != -1 )
    {
        clearWebRequestSerial( mWebRequest );
    }
}


char StatisticsPage::getReturnToMenu()
{
    return mReturnToMenu;
}


void StatisticsPage::actionPerformed( GUIComponent *inTarget )
{
    if( inTarget == &mMenuButton )
    {
        mReturnToMenu = true;
    }
    else if( inTarget == &mCategorie1Button )
    {
        mCurrentHighlightedButton->setIsHighlighting( false );
        mCategorie1Button.setIsHighlighting( true, false, -1 );
        mCurrentHighlightedButton = &mCategorie1Button;
        
        mStatisticAttributesOffset = 0 * NUM_ENTRIES_PER_PAGE;
        mCategorieChanged = true;
    }
    else if( inTarget == &mCategorie2Button )
    {
        mCurrentHighlightedButton->setIsHighlighting( false );
        mCategorie2Button.setIsHighlighting( true, false, -1 );
        mCurrentHighlightedButton = &mCategorie2Button;
    
        mStatisticAttributesOffset = 1 * NUM_ENTRIES_PER_PAGE;
        mCategorieChanged = true;
    }
    else if( inTarget == &mCategorie3Button )
    {
        mCurrentHighlightedButton->setIsHighlighting( false );
        mCategorie3Button.setIsHighlighting( true, false, -1 );
        mCurrentHighlightedButton = &mCategorie3Button;
    
        mStatisticAttributesOffset = 2 * NUM_ENTRIES_PER_PAGE;
        mCategorieChanged = true;
    }
    else if( inTarget == &mCategorie4Button )
    {
        mCurrentHighlightedButton->setIsHighlighting( false );
        mCategorie4Button.setIsHighlighting( true, false, -1 );
        mCurrentHighlightedButton = &mCategorie4Button;
    
        mStatisticAttributesOffset = 3 * NUM_ENTRIES_PER_PAGE;
        mCategorieChanged = true;
    }
    else if( inTarget == &mCategorie5Button )
    {
        mCurrentHighlightedButton->setIsHighlighting( false );
        mCategorie5Button.setIsHighlighting( true, false, -1 );
        mCurrentHighlightedButton = &mCategorie5Button;
    
        mStatisticAttributesOffset = 4 * NUM_ENTRIES_PER_PAGE;
        mCategorieChanged = true;
    }
    
}


void StatisticsPage::step()
{
    if( mWebRequest != -1 )
    {
        int stepResult = stepWebRequestSerial( mWebRequest );
        
        if( stepResult != 0 )
        {
            setWaiting( false );
            mMenuButton.setVisible( true );
        }
        
        switch( stepResult )
        {
            case 0:
                break;
                
            case -1:
                mStatusError = true;
                mStatusMessageKey = "err_webRequest";
                clearWebRequestSerial( mWebRequest );
                mWebRequest = -1;
                break;
                
            case 1: {
                char *result = getWebResultSerial( mWebRequest );
                clearWebRequestSerial( mWebRequest );
                mWebRequest = -1;
                     
                printf( "Web result = %s\n", result );
                
                if( strstr( result, "DENIED" ) != NULL )
                {
                    mStatusError = true;
                    mStatusMessageKey = "showStatisticsFailed";
                }
                else
                {
                    SimpleVector<char *> *tokens = tokenizeString( result );
                    
                    int correctNumTokens = NUM_STATISTIC_ATTRIBUTES + NUM_STATISTIC_ATTRIBUTES * NUM_STATISTIC_TABLES + 1;
                    
                    if( tokens->size() == correctNumTokens && strcmp( *( tokens->getElement( correctNumTokens - 1 ) ), "OK" ) == 0 )
                    {
                        for( int i = 0; i < NUM_STATISTIC_ATTRIBUTES; ++i )
                        {
                            mStatisticAttributeNames[ i ] = stringDuplicate( *( tokens->getElement( i ) ) );
                            
                            sscanf( *( tokens->getElement( i + NUM_STATISTIC_ATTRIBUTES ) ), "%d", mStatisticsCurrentLife + i );
                            
                            sscanf( *( tokens->getElement( i + NUM_STATISTIC_ATTRIBUTES * NUM_STATISTIC_TABLES ) ), "%d", mStatisticsOverall + i );
                        }
                    }
                    else
                    {
                        mStatusError = true;
                        mStatusMessageKey = "err_badServerResponse";
                    }
                }
                break;
            }
        }
    }
}

        
void StatisticsPage::makeActive( char inFresh )
{
    if( !inFresh )
    {
        return;
    }
    
    char *ticketHash = getTicketHash();
    
    char *actionString = autoSprintf(
        "action=show_player_statistics"
        "&user_id=%d"
        "&%s",
        userID, ticketHash );
    
    delete[] ticketHash;
    
    mCategorie1Button.setVisible( false );
    mCategorie2Button.setVisible( false );
    mCategorie3Button.setVisible( false );
    mCategorie4Button.setVisible( false );
    mCategorie5Button.setVisible( false );    
    
    // send the web request
    mWebRequest = startWebRequestSerial( "POST", 
                                   serverURL, 
                                   actionString );

    delete[] actionString;

    // reset all data
    mReturnToMenu = false;
    
    // set view to first page
    mStatisticAttributesOffset = 0;
    if( mCurrentHighlightedButton )
    {
        mCurrentHighlightedButton->setIsHighlighting( false );
    }
    mCategorie1Button.setIsHighlighting( true, false, -1 );
    mCurrentHighlightedButton = &mCategorie1Button;
    
    mCategorieChanged = false;
    
    mStatusError = false;
    mStatusMessageKey = NULL;

    setStatusDirect( NULL, false );

    mMenuButton.setVisible( false );

    setWaiting( true );
}


void StatisticsPage::draw( doublePair inViewCenter,
                                double inViewSize )
{
    if( !mMenuButton.isVisible() || mStatusError )
    {
        return;
    }
    
    mCategorie1Button.setVisible( true );
    mCategorie2Button.setVisible( true );
    mCategorie3Button.setVisible( true );
    mCategorie4Button.setVisible( true );
    mCategorie5Button.setVisible( true );

    doublePair labelPos = { -8, 4.5 };
    doublePair valuesPos1 = { 3.5, 4.5 };
    doublePair valuesPos2 = { 7, 4.5 };
    
    drawMessage( translate( "statisticsAttribute" ), labelPos, false, 1.0, alignLeft, true );
    
    if( mStatisticAttributesOffset < 4 * NUM_ENTRIES_PER_PAGE )
    {
        drawMessage( translate( "statisticsCurrentLife" ), valuesPos1, false, 1.0, alignCenter, true );
        drawMessage( translate( "statisticsOverall" ), valuesPos2, false, 1.0, alignCenter, true );
    }
    else
    {
        drawMessage( translate( "statisticsOverall" ), valuesPos1, false, 1.0, alignCenter, true );
    }

    labelPos.y -= 1.5;
    valuesPos1.y -= 1.5;
    valuesPos2.y -= 1.5;
    
    int maxAttributeID = mStatisticAttributesOffset + NUM_ENTRIES_PER_PAGE;
    if( maxAttributeID > NUM_STATISTIC_ATTRIBUTES )
    {
        maxAttributeID = NUM_STATISTIC_ATTRIBUTES;
    }
    
    for( int i = mStatisticAttributesOffset; i < maxAttributeID; ++i )
    {
        int index = statisticsOrder[ i ];
        if( index != -1 )
        {
            char* attributeName = autoSprintf( "%s", translate( mStatisticAttributeNames[ index ] ) );
            
            char* value1;
            char* value2;
            if( isAMoneyStatistic[ i ] )
            {
                value1 = autoSprintf( "$%d", mStatisticsCurrentLife[ index ] );
                value2 = autoSprintf( "$%d", mStatisticsOverall[ index ] );
            }
            else
            {
                value1 = autoSprintf( "%d", mStatisticsCurrentLife[ index ] );
                value2 = autoSprintf( "%d", mStatisticsOverall[ index ] );
            }

            drawMessage( attributeName , labelPos, false, 1.0, alignLeft );
            
            // on this page draw only overall values
            //   (since all infos about "deaths" will always
            //    be zero if the "current life" is examined)
            if( mStatisticAttributesOffset < 4 * NUM_ENTRIES_PER_PAGE )
            {
                drawMessage( value1 , valuesPos1, false );
                drawMessage( value2 , valuesPos2, false );
            }
            else
            {
                drawMessage( value2 , valuesPos1, false );
            }
            
            delete [] attributeName;
            delete [] value1;
            delete [] value2;
        }
        
        labelPos.y -= 1;
        valuesPos1.y -= 1;
        valuesPos2.y -= 1;
    }
}

char StatisticsPage::getHasCategoryChanged()
{
    return mCategorieChanged;
}


