#include "GamePage.h"

#include "TextField.h"
#include "TextButton.h"
#include "inventory.h"

#include "minorGems/ui/event/ActionListener.h"


class CompetitionCheckinPage : public GamePage, public ActionListener {
        
    public:
        
        CompetitionCheckinPage();
        
        virtual ~CompetitionCheckinPage();


        virtual char getReturnToHome();
        
        virtual void setSuccess( int inSuccess );
        virtual void setCompetitionID( int inCompetitionID );

        // destoryed by caller
        virtual void setPriceList( char *inPriceList );
        virtual void setMoveList( char *inMoveList );

        virtual void actionPerformed( GUIComponent *inTarget );
        


        virtual void step();
        
        virtual void makeActive( char inFresh );


    protected:

        int mWebRequest;

        char *mPriceList;
        char *mMoveList;
        
        int mSuccess;
        int mCompetitionID;

        int mIsHighscore;

        TextButton mHomeButton;
        
        char mReturnToHome;

    };

