#include "GamePage.h"

#include "TextField.h"
#include "TextButton.h"


#include "minorGems/ui/event/ActionListener.h"


class CompetitionCheckoutPage : public GamePage, public ActionListener {
        
    public:
        
        CompetitionCheckoutPage();
        
        virtual ~CompetitionCheckoutPage();

        // must be called before makeActive
        void setCompetitionID( int inID );

        virtual char getReturnToMenu();
        

        // destroyed by caller if not NULL
        // can only be called once per checkout (because this
        // call destroys/clears the internal house map)
        virtual char *getHouseMap();
        
        virtual int getTimeout();
        virtual int getMusicSeed();

        virtual int getStepCost();
        virtual int getBestScore();
        virtual int getCompetitionID();

        // destroyed by caller if not NULL
        virtual char *getPriceList();

        virtual void actionPerformed( GUIComponent *inTarget );


        virtual void step();
        
        virtual void makeActive( char inFresh );

    protected:
        void clearDataMembers();
        
        int mWebRequest;
        
        char *mHouseMap;
        int mTimeout;
        int mMusicSeed;

        int mStepCost;
        int mBestScore;

        char *mPriceList;

        TextButton mMenuButton;


        char mReturnToMenu;
        
        int mCompetitionID;
    };

