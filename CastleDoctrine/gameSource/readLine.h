#ifndef READ_LINE_INCLUDED
#define READ_LINE_INCLUDED

#include "minorGems/network/Socket.h"

// reads newline terminated string
// returns result WITHOUT newline at end (in fact, trimmed of whitespace)
// returns NULL on failure.
// Keeps reading until what it's read ends with a newline,
// so protocol should involve sending one line then waiting for a response.
// If 'nonblocking', return NULL if there's nothing to read - but once we've
// read something, we continue to read, blocking, until we find a newline.
char *readLine( Socket *inSock, bool nonblocking=false );

#endif
