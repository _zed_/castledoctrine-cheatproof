#include "ShowUserMapPage.h"

#include "message.h"
#include "balance.h"


#include "minorGems/game/Font.h"
#include "minorGems/game/game.h"
#include "minorGems/game/drawUtils.h"

#include "minorGems/util/stringUtils.h"
#include "minorGems/util/SettingsManager.h"


extern Font *mainFont;


ShowUserMapPage::ShowUserMapPage()
        : mUserMap( NULL ),
          mObjectPicker( 8, 5 ),
          mGridDisplay( 0, 0, &mObjectPicker ),
          mDoneButton( mainFont, 8, -5, translate( "doneEdit" ) ),
          mDone( false ),
          mMapNumber( 0 ),
          mHouseCost( 0 ),
          mSavedCost( 0 ),
          mTotalCost( 0 )
{

    addComponent( &mDoneButton );
    addComponent( &mObjectPicker );
    addComponent( &mGridDisplay );
    
    mDoneButton.setMouseOverTip( "" );
    
    mDoneButton.addActionListener( this );
    mObjectPicker.addActionListener( this );
}


        
ShowUserMapPage::~ShowUserMapPage()
{
    if( mUserMap != NULL )
    {
        delete [] mUserMap;
    }
}


void ShowUserMapPage::setUserMap( const char *inUserMap )
{
    if( mUserMap != NULL )
    {
        delete [] mUserMap;
    }
    mUserMap = stringDuplicate( inUserMap );
    
    mGridDisplay.setHouseMap( mUserMap );
    mGridDisplay.setWifeName( "Wife" );
    mGridDisplay.setSonName( "Son" );
    mGridDisplay.setDaughterName( "Daughter" );
    mGridDisplay.setIsUserMap( true );
    mObjectPicker.setIsUserMap( true );
}


char* ShowUserMapPage::getUserMap()
{
    return mGridDisplay.getHouseMap();
}


HouseObjectPicker* ShowUserMapPage::getObjectPicker()
{
    return &mObjectPicker;
}


char ShowUserMapPage::getDone()
{
    return mDone;
}


void ShowUserMapPage::actionPerformed( GUIComponent *inTarget )
{
    if( inTarget == &mDoneButton )
    {
        mDone = true;
    }
}

        
void ShowUserMapPage::makeActive( char inFresh )
{
    if( !inFresh )
    {
        return;
    }
    
    mGridDisplay.clearMovementKeyHolds();
    
    mDone = false;
}


void ShowUserMapPage::keyDown( unsigned char inASCII )
{
    if( inASCII == '+' )
    {
        mGridDisplay.saveWholeMapImage();
    }
}


void ShowUserMapPage::setUserMapInfo( int mapNumber, int houseCost, int savedCost, int totalCost )
{
    mMapNumber = mapNumber;
    mHouseCost = houseCost;
    mSavedCost = savedCost;
    mTotalCost = totalCost;
}


void ShowUserMapPage::draw( doublePair inViewCenter, double inViewSize ) {
        
    doublePair labelPos = { -4, 7 };
    
    char* costString = autoSprintf( translate( "showUserMapHouseCostsAndCostsSaved" ),
                                    mHouseCost, mSavedCost );
    
    char* totalCostString = autoSprintf( translate( "showUserMapHouseCostsTotal" ),
                                                    mTotalCost );

    drawMessage( costString, labelPos, false );
    labelPos.x = 4;
    labelPos.y = 6.6;
    drawMessage( totalCostString, labelPos, false );
    
    labelPos.x = 0;
    labelPos.y = -6.25;
    char *mapNumberString = autoSprintf( translate( "showUserMapMapNumber" ), mMapNumber + 1 );
    drawMessage( mapNumberString, labelPos, false );

    delete [] costString;
    delete [] totalCostString;
    delete [] mapNumberString;
}



