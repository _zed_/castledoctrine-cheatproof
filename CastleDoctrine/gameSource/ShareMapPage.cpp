#include "ShareMapPage.h"
#include "ticketHash.h"
#include "message.h"

#include "serialWebRequests.h"

#include "minorGems/game/Font.h"
#include "minorGems/game/game.h"

#include "minorGems/util/stringUtils.h"
#include "minorGems/network/web/URLUtils.h"


extern Font *mainFont;

extern char *serverURL;

extern int userID;

ShareMapPage::ShareMapPage()
        : mWebRequest( -1 ),
          mDoneButton( mainFont, 4, -4, translate( "doneEdit" ) ),
          mDone( false ),
          mIsShareMapAfterDeath( false )
{
    addComponent( &mDoneButton );
    mDoneButton.addActionListener( this );
    mDoneButton.setMouseOverTip( "" );
}

        
ShareMapPage::~ShareMapPage()
{
    if( mWebRequest != -1 )
    {
        clearWebRequestSerial( mWebRequest );
    }
}


char ShareMapPage::getDone()
{
    return mDone;
}


void ShareMapPage::actionPerformed( GUIComponent *inTarget )
{
    if( inTarget == &mDoneButton )
    {
        mDone = true;
    }
}


void ShareMapPage::step()
{
    if( mWebRequest != -1 )
    {
        int stepResult = stepWebRequestSerial( mWebRequest );
        
        if( stepResult != 0 )
        {
            setWaiting( false );
            mDoneButton.setVisible( true );
        }
        
        switch( stepResult )
        {
            case 0:
                break;
                
            case -1:
                mStatusError = true;
                mStatusMessageKey = "err_webRequest";
                clearWebRequestSerial( mWebRequest );
                mWebRequest = -1;
                break;
                
            case 1: {
                char *result = getWebResultSerial( mWebRequest );
                clearWebRequestSerial( mWebRequest );
                mWebRequest = -1;
                     
                printf( "Web result = %s\n", result );
                
                if( !mIsShareMapAfterDeath )
                {
                    if( strstr( result, "DENIED" ) != NULL )
                    {
                        mStatusError = true;
                        mStatusMessageKey = "shareMapFailed";
                    }
                    else if( strstr( result, "EDIT_COUNT_BELOW_ZERO" ) != NULL )
                    {
                        mStatusError = true;
                        mStatusMessageKey = "shareMapFailedEditCount";
                    }
                    else if( strstr( result, "HOUSE_BEING_ROBBED" ) != NULL )
                    {
                        mStatusError = true;
                        mStatusMessageKey = "shareMapFailedRobbery";
                    }
                    else if( strstr( result, "NOT_ENOUGH_MOVES" ) != NULL )
                    {
                        mStatusError = true;
                        mStatusMessageKey = "shareMapFailedNotEnoughMoves";
                    }
                    else if( strstr( result, "NOT_ENOUGH_ROBBER_DEATHS" ) != NULL )
                    {
                        mStatusError = true;
                        mStatusMessageKey = "shareMapFailedRobberDeaths";
                    }
                    else if( strstr( result, "MAP_EXISTS_ALREADY" ) != NULL )
                    {
                        mStatusError = true;
                        mStatusMessageKey = "shareMapFailedMapExistsAlready";
                    }
                    else if( strstr( result, "COST_TOO_LOW" ) != NULL )
                    {
                        mStatusError = true;
                        mStatusMessageKey = "shareMapFailedCostTooLow";
                    }
                    else if( strstr( result, "OK" ) != NULL )
                    {
                        mStatusError = false;
                    }
                    else
                    {
                        mStatusError = true;
                        mStatusMessageKey = "err_badServerResponse";
                    }
                }
                else
                {
                    SimpleVector<char *> *lines = tokenizeString( result );
                    
                    if( strcmp( result, "DENIED" ) == 0 )
                    {
                        mStatusError = true;
                        mStatusMessageKey = "shareMapAfterDeathFailed";
                    }
                    else if( lines->size() == 2 && strstr( result, "BOT_MAP_USAGE_DENIED" ) != NULL )
                    {
                        sscanf( *( lines->getElement( 0 ) ),
                                "%d", &mBotMapsDeleted );
                    
                        mStatusError = false; // no error, user simply did not allow
                        mDone = true; // leave page without further user interaction
                    }
                    else if( lines->size() == 1 && strstr( result, "CANDIDATE_BOT_MAP_NOT_FOUND" ) != NULL )
                    {
                        if( mIsShareMapAfterDeathAllowed )
                        {
                            mStatusError = true;
                            mStatusMessageKey = "sharedMapAfterDeathMapNotFound";
                        }
                        else
                        {
                            // map was not found, but it does not matter
                            // user does not want to share it,
                            // so we would delete it anyway
                            mStatusError = false;
                            mDone = true; // leave page without further user interaction
                        }
                    }
                    else if( lines->size() == 3 && strstr( result, "OK" ) != NULL )
                    {
                        sscanf( *( lines->getElement( 0 ) ),
                                "%d", &mBotMapsDeleted );
                        sscanf( *( lines->getElement( 0 ) ),
                                "%d", &mBotMapsAdded );
                    
                        mStatusError = false;
                    }
                    else
                    {
                        mStatusError = true;
                        mStatusMessageKey = "err_badServerResponse";
                    }
                    
                    for( int i = 0; i < lines->size(); ++i )
                    {
                        delete [] *( lines->getElement( i ) );
                    }
                    delete lines;
                }
                break;
            }
        }
    }
}

        
void ShareMapPage::makeActive( char inFresh )
{
    if( !inFresh )
    {
        return;
    }
    
    char *ticketHash = getTicketHash();
    
    char *actionString;
    
    if( mIsShareMapAfterDeath )
    {
        actionString = autoSprintf(
            "action=share_map_after_death"
            "&user_id=%d&usage_allowed=%d"
            "&%s",
            userID, mIsShareMapAfterDeathAllowed, ticketHash );
    }
    else
    {
        actionString = autoSprintf(
            "action=share_map"
            "&user_id=%d"
            "&%s",
            userID, ticketHash );
    }
    
    delete[] ticketHash;
    
    // send the web request
    mWebRequest = startWebRequestSerial( "POST", 
                                         serverURL,
                                         actionString );

    delete[] actionString;

    // reset all data
    mDone = false;
    mStatusError = false;
    mStatusMessageKey = NULL;

    setStatusDirect( NULL, false );

    mDoneButton.setVisible( false );
    
    setWaiting( true );
}


void ShareMapPage::draw( doublePair inViewCenter,
                                double inViewSize )
{
    if( ! mDoneButton.isVisible() )
    {
        return;
    }
    
    doublePair labelPos = { 0, 4 };

    char* message = NULL;
    if( mStatusError && mIsShareMapAfterDeathAllowed )
    {
        message = autoSprintf( "%s", translate( "shareMapAfterDeathFailed" ) );
    }
    else if( mStatusError && !mIsShareMapAfterDeathAllowed )
    {
        message = autoSprintf( "%s", translate( "shareMapFailed" ) );
    }
    else if( !mStatusError && mIsShareMapAfterDeathAllowed )
    {
        message = autoSprintf( "%s", translate( "shareMapAfterDeathSuccess" ) );
    }
    else if( !mStatusError && !mIsShareMapAfterDeathAllowed )
    {
        message = autoSprintf( "%s", translate( "shareMapSuccess" ) );
    }
    
    drawMessage( message, labelPos, false );
}


void ShareMapPage::setIsShareMapAfterDeath( char isShareMapAfterDeath, char isShareMapAfterDeathAllowed )
{
    mIsShareMapAfterDeath = isShareMapAfterDeath;
    mIsShareMapAfterDeathAllowed = isShareMapAfterDeathAllowed;
}

char ShareMapPage::getIsShareMapAfterDeath()
{
    return mIsShareMapAfterDeath;
}

char ShareMapPage::getIsShareMapAfterDeathAllowed()
{
    return mIsShareMapAfterDeathAllowed;
}

