#include "GamePage.h"

#include "TextField.h"
#include "TextButton.h"

#include "minorGems/ui/event/ActionListener.h"


class StatisticsPage : public GamePage, public ActionListener
{
        
    public:
        
        StatisticsPage();
        
        virtual ~StatisticsPage();

        virtual char getReturnToMenu();
    
        virtual void actionPerformed( GUIComponent *inTarget );

        virtual void step();
        
        virtual void draw( doublePair inViewCenter, 
                           double inViewSize );
        
        virtual void makeActive( char inFresh );
    
        char getHasCategoryChanged();
    
    protected:
    
        static const int NUM_STATISTIC_ATTRIBUTES = 40;
        static const int NUM_STATISTIC_TABLES = 2;
        static const int NUM_PAGES = 5;
        static const int NUM_ENTRIES_PER_PAGE = 8;
    
        static const int statisticsOrder[ NUM_STATISTIC_ATTRIBUTES ];    
        static const int isAMoneyStatistic[ NUM_STATISTIC_ATTRIBUTES ];

        int mWebRequest;
    
        TextButton mMenuButton;
    
        char mReturnToMenu;
    
        TextButton mCategorie1Button;
        TextButton mCategorie2Button;
        TextButton mCategorie3Button;
        TextButton mCategorie4Button;
        TextButton mCategorie5Button;
    
        char mCategorieChanged;
    
        int mStatisticsCurrentLife[ NUM_STATISTIC_ATTRIBUTES ];
        int mStatisticsOverall[ NUM_STATISTIC_ATTRIBUTES ];
        char* mStatisticAttributeNames[ NUM_STATISTIC_ATTRIBUTES ];
    
        int mStatisticAttributesOffset;
    
        TextButton* mCurrentHighlightedButton;

};

