#include "BotMapVotePage.h"
#include "ticketHash.h"
#include "message.h"

#include "serialWebRequests.h"

#include "minorGems/game/Font.h"
#include "minorGems/game/game.h"

#include "minorGems/util/stringUtils.h"
#include "minorGems/network/web/URLUtils.h"


extern Font *mainFont;

extern char *serverURL;

extern int userID;

BotMapVotePage::BotMapVotePage()
        : mWebRequest( -1 ),
          mReturnToHome( false ),
          mBotMapUserVote( 0 ),
          mBotMapUserID( 0 )
{
}

        
BotMapVotePage::~BotMapVotePage()
{
    if( mWebRequest != -1 )
    {
        clearWebRequestSerial( mWebRequest );
    }
}


char BotMapVotePage::getReturnToHome()
{
    return mReturnToHome;
}

void BotMapVotePage::step()
{
    if( mWebRequest != -1 )
    {
        int stepResult = stepWebRequestSerial( mWebRequest );
        
        if( stepResult != 0 )
        {
            setWaiting( false );
        }
        
        switch( stepResult )
        {
            case 0:
                break;
            
            case -1:
                mStatusError = true;
                mStatusMessageKey = "err_webRequest";
                clearWebRequestSerial( mWebRequest );
                mWebRequest = -1;
                break;
                
            case 1: {
                char *result = getWebResultSerial( mWebRequest );
                clearWebRequestSerial( mWebRequest );
                mWebRequest = -1;
                     
                printf( "Web result = %s\n", result );
                
                if( strcmp( result, "OK" ) == 0 )
                {
                    mReturnToHome = true;
                }
                else //if( strstr( result, "DENIED" ) != NULL )
                {
                    mStatusError = true;
                    mStatusMessageKey = "botMapUserVoteFailed";
                }
                break;
            }
        }
    }
}


void BotMapVotePage::actionPerformed( GUIComponent *inTarget )
{
//    if( inTarget == &mHomeButton )
//    {
//        mReturnToHome = true;
//    }
}

        
void BotMapVotePage::makeActive( char inFresh )
{
    if( !inFresh )
    {
        return;
    }
    
    char *ticketHash = getTicketHash();
    
    char *actionString = autoSprintf(
        "action=bot_map_vote_user"
        "&user_id=%d&bot_map_user_vote=%d&bot_map_owner=%d"
        "&%s",
         userID, mBotMapUserVote, mBotMapUserID, ticketHash );
    
    delete[] ticketHash;
    
    // send the web request
    mWebRequest = startWebRequestSerial( "POST", 
                                   serverURL, 
                                   actionString );

    delete[] actionString;
    
    // reset all data
    mReturnToHome = false;
    mBotMapUserVote = 0;
    mBotMapUserID = 0;

    mStatusError = false;
    mStatusMessageKey = NULL;

    setWaiting( true );
}


void BotMapVotePage::setBotMapInfos( int botMapUserID, int botMapUserVote )
{
    mBotMapUserVote = botMapUserVote;
    mBotMapUserID = botMapUserID;
}








