#ifndef ROB_HOUSE_SERVER_INCLUDED
#define ROB_HOUSE_SERVER_INCLUDED


#include "RobHouseGridDisplay.h"
#include "inventory.h"

#include "minorGems/util/SimpleVector.h"

class RobHouseServer : public RobHouseGridDisplay {
    
    public:

        RobHouseServer();
        
        virtual ~RobHouseServer();

        virtual void draw() {}
        virtual void keyDown( unsigned char inASCII ) {}

        char *handleMove( const char *move );
        
        virtual void setHouseMap( const char *inHouseMap );

        void setBackpackSlots( SimpleVector<QuantityRecord> backpackSlots );
        SimpleVector<QuantityRecord> getBackpackSlots();

    protected:
        
        int *mClientHouseMapIDs;
        int *mClientHouseMapCellStates;
        int *mClientHouseMapMobileIDs;
        int *mClientHouseMapMobileCellStates;
        char *mTileWasRevealedMap;

        bool mClientDead;

        SimpleVector<QuantityRecord> mBackpackSlots;
    };



#endif
