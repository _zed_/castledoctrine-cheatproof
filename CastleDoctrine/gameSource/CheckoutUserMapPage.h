#include "GamePage.h"

#include "TextField.h"
#include "TextButton.h"


#include "minorGems/ui/event/ActionListener.h"


class CheckoutUserMapPage : public GamePage, public ActionListener {
        
    public:
        
        CheckoutUserMapPage();
        
        virtual ~CheckoutUserMapPage();

        virtual void setMapNumber( int mapNumber );

        virtual char getReturnToMenu();
    
        // destroyed by caller if not NULL
        virtual char *getUserMap();
    
        virtual void actionPerformed( GUIComponent *inTarget );


        virtual void step();
        
        virtual void makeActive( char inFresh );
    
        int getHouseCost();
        int getSavedCost();
        int getTotalCost();
        int getMapNumber();

    protected:

        int mWebRequest;
   
        char *mUserMap;
        int mMapNumber;

        TextButton mMenuButton;

        char mReturnToMenu;
    
        int mHouseCost;
        int mSavedCost;
        int mTotalCost;

    };

