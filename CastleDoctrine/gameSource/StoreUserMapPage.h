#include "GamePage.h"

#include "TextField.h"
#include "TextButton.h"

#include "minorGems/ui/event/ActionListener.h"


class StoreUserMapPage : public GamePage, public ActionListener
{
        
    public:
        
        StoreUserMapPage();
        
        virtual ~StoreUserMapPage();

        virtual char getReturnToHome();
    
        virtual void actionPerformed( GUIComponent *inTarget );

        virtual void step();
        
        virtual void draw( doublePair inViewCenter, 
                           double inViewSize );
        
        virtual void makeActive( char inFresh );
    
        void setIsStoringMode( char isStoringMode );
    
        char getIsStoringMode();
    
        void setMapNumber( int mapNumber );
    
    protected:

        int mWebRequest;
    
        TextButton mHomeButton;
    
        char mReturnToHome;
    
        int mHouseCost, mCostSaved, mTotalCost, mMissingCash;
    
        char mIsStoringMode;
    
        char mSuccess;
    
        int mMapNumber;

};

