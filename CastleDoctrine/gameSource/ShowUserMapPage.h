#include "GamePage.h"

#include "TextField.h"
#include "TextButton.h"
#include "KeyEquivalentTextButton.h"
#include "SpriteToggleButton.h"
#include "CheckboxButton.h"

#include "HouseGridDisplay.h"
#include "HouseObjectPicker.h"

#include "Gallery.h"

#include "minorGems/ui/event/ActionListener.h"




class ShowUserMapPage : public GamePage, public ActionListener {
        
    public:
        
        ShowUserMapPage();
        
        virtual ~ShowUserMapPage();
    
        // destroyed by caller
        virtual void setUserMap( const char *inUserMap );
        virtual char *getUserMap();
    
        char getDone();

        virtual void actionPerformed( GUIComponent *inTarget );
    
        
        virtual void makeActive( char inFresh );


        virtual void keyDown( unsigned char inASCII );
    
        virtual void draw( doublePair inViewCenter, double inViewSize );

        HouseObjectPicker *getObjectPicker();
    
        void setUserMapInfo( int mapNumber, int houseCost, int savedCost, int totalCost );

    protected:
    
        char *mUserMap;

        HouseObjectPicker mObjectPicker;
        HouseGridDisplay mGridDisplay;
        TextButton mDoneButton;
        char mDone;
    
        int mMapNumber;
    
        int mHouseCost;
        int mSavedCost;
        int mTotalCost;        

    };
