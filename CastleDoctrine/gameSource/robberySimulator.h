#include "inventory.h"
#include "minorGems/util/SimpleVector.h"
#include "minorGems/network/Socket.h"


// returns response also described in headlessProtocol.txt.
// (Without the [END_REQUEST] or [END_RESPONSE] markers.) 
// response is FAILED if robbery contains an illegal move
char *simulateRobbery( const char *houseMap,
    SimpleVector<QuantityRecord> backpackSlots,
    int wifeMoney,
    char *moveList );

struct RobData {
    RobData(const char *i_houseMap,
            SimpleVector<QuantityRecord> i_backpackSlots,
            int i_wifeMoney) :
        backpackSlots(i_backpackSlots), wifeMoney(i_wifeMoney) {
            houseMap = stringDuplicate(i_houseMap);
        }
    ~RobData() { delete [] houseMap; }
    const char *houseMap;
    SimpleVector<QuantityRecord> backpackSlots;
    int wifeMoney;
    };

struct RobResult {
    RobResult( char i_success,
            char i_wifeKilledRobber,
            char i_wifeKilled,
            char i_wifeRobbed,
            char i_familyKilledCount,
            const char *i_backpack,
            const char *i_houseMap,
            const char *i_moveList ) :
        success(i_success),
        wifeKilledRobber(i_wifeKilledRobber),
        wifeKilled(i_wifeKilled),
        wifeRobbed(i_wifeRobbed),
        familyKilledCount(i_familyKilledCount) {
            backpack = stringDuplicate( i_backpack );
            houseMap = stringDuplicate( i_houseMap );
            moveList = stringDuplicate( i_moveList );
            }

    ~RobResult() {
        delete [] moveList;
        delete [] backpack;
        delete [] houseMap;
        }

    char success;
    char wifeKilledRobber;
    char wifeKilled;
    char wifeRobbed;
    char familyKilledCount;
    char *moveList;
    char *backpack;
    char *houseMap;
    };

RobResult *doRob( const RobData *data, Socket *sock );
