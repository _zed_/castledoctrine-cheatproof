#include "CheckoutUserMapPage.h"
#include "ticketHash.h"
#include "message.h"
#include "LiveHousePage.h"

#include "serialWebRequests.h"



#include "minorGems/game/Font.h"
#include "minorGems/game/game.h"

#include "minorGems/util/stringUtils.h"


extern Font *mainFont;


extern char *serverURL;

extern int userID;



CheckoutUserMapPage::CheckoutUserMapPage()
        : mWebRequest( -1 ),
          mUserMap( NULL ),
          mMapNumber( 0 ),
          mMenuButton( mainFont, 4, -4, translate( "returnMenu" ) ),
          mReturnToMenu( false ),
          mHouseCost( 0 ),
          mSavedCost( 0 ),
          mTotalCost( 0 )
{
    addComponent( &mMenuButton );
    mMenuButton.addActionListener( this );
}


        
CheckoutUserMapPage::~CheckoutUserMapPage()
{
    if( mWebRequest != -1 )
    {
        clearWebRequestSerial( mWebRequest );
    }
    if( mUserMap != NULL )
    {
        delete [] mUserMap;
    }
}


char CheckoutUserMapPage::getReturnToMenu()
{
    return mReturnToMenu;
}

void CheckoutUserMapPage::setMapNumber( int mapNumber )
{
    mMapNumber = mapNumber;
}


char *CheckoutUserMapPage::getUserMap()
{
    if( mUserMap == NULL )
    {
        return NULL;
    }
    else
    {
        return stringDuplicate( mUserMap );
    }
}


int CheckoutUserMapPage::getHouseCost()
{
    return mHouseCost;
}


int CheckoutUserMapPage::getSavedCost()
{
    return mSavedCost;
}


int CheckoutUserMapPage::getTotalCost()
{
    return mTotalCost;
}


int CheckoutUserMapPage::getMapNumber()
{
    return mMapNumber;
}


void CheckoutUserMapPage::actionPerformed( GUIComponent *inTarget )
{
    if( inTarget == &mMenuButton )
    {
        mReturnToMenu = true;
    }
}


void CheckoutUserMapPage::step()
{
    if( mWebRequest != -1 )
    {
        int stepResult = stepWebRequestSerial( mWebRequest );
        
        if( stepResult != 0 )
        {
            setWaiting( false );
        }
        
        switch( stepResult )
        {
            case 0:
                break;
            case -1:
                mStatusError = true;
                mStatusMessageKey = "err_webRequest";
                clearWebRequestSerial( mWebRequest );
                mWebRequest = -1;
                mMenuButton.setVisible( true );
                break;
            case 1: {
                char *result = getWebResultSerial( mWebRequest );
                clearWebRequestSerial( mWebRequest );
                mWebRequest = -1;
                     
                printf( "Web result = %s\n", result );
   
                SimpleVector<char *> *lines = tokenizeString( result );
                
                if( lines->size() == 5 && strcmp( *( lines->getElement( 4 ) ), "OK" ) == 0 )
                {
                    mUserMap   = *( lines->getElement( 0 ) );
                    sscanf( *( lines->getElement( 1 ) ), "%d", &mHouseCost );
                    sscanf( *( lines->getElement( 2 ) ), "%d", &mSavedCost );
                    sscanf( *( lines->getElement( 3 ) ), "%d", &mTotalCost );
                    
                    printf( "HouseMap = %s\n", mUserMap );
                    printf( "HouseCost = %d\n", mHouseCost );
                    printf( "SavedCost = %d\n", mSavedCost );
                    printf( "TotalCost = %d\n", mTotalCost );
                    
                    // delete all strings except the first one
                    for( int i = 1; i < lines->size(); i++ )
                    {
                        delete [] *( lines->getElement( i ) );
                    }
                }
                else
                {
                    if( lines->size() == 1 && strcmp( *( lines->getElement( 0 ) ), "MISSING_MAP" ) == 0 )
                    {
                        mStatusMessageKey = "showUserMapFailedMapMissing";
                    }
                    else
                    {
                        setStatus( "err_badServerResponse", true );
                    }
                    
                    mMenuButton.setVisible( true );
                    mStatusError = true;

                    for( int i = 0; i < lines->size(); i++ )
                    {
                        delete [] *( lines->getElement( i ) );
                    }
                }
                
                delete lines;
                delete [] result;
            }
            break;
        }
    }
}


        
void CheckoutUserMapPage::makeActive( char inFresh )
{
    if( !inFresh )
    {
        return;
    }

    if( mUserMap != NULL )
    {
        delete [] mUserMap;
    }
    mUserMap = NULL;
    
    mHouseCost = 0;
    mSavedCost = 0;
    mTotalCost = 0;
   
    char *ticketHash = getTicketHash();

    char *fullRequestURL = autoSprintf( 
        "%s?action=show_user_map&user_id=%d&map_number=%d"
        "&%s",
        serverURL, userID, mMapNumber, ticketHash );
    delete [] ticketHash;
    
    mWebRequest = startWebRequestSerial( "GET", 
                                         fullRequestURL,
                                         NULL );
    
    delete [] fullRequestURL;
    
    mMenuButton.setVisible( false );

    mStatusError = false;
    mStatusMessageKey = NULL;

    mReturnToMenu = false;

    setWaiting( true );
}

