#include "CompetitionsMenuPage.h"

#include "RobPickList.h"

#include "minorGems/game/game.h"


extern Font *mainFont;





CompetitionsMenuPage::CompetitionsMenuPage() 
        : mPickList( 0, 0, COMPETITIONS, this ),
          mMenuButton( mainFont, 4, -4, translate( "returnMenu" ) ),
          mStartCompetitionButton( mainFont, -4, -4, translate( "startCompetition" ) ),
          mReturnToMenu( false ),
          mStartCompetition( false ) {

    addComponent( &mMenuButton );
    addComponent( &mStartCompetitionButton );
    
    addComponent( &mPickList );
    

    mMenuButton.addActionListener( this );
    mStartCompetitionButton.addActionListener( this );

    mPickList.addActionListener( this );

    mStartCompetitionButton.setVisible( false );
    }


        
CompetitionsMenuPage::~CompetitionsMenuPage() {
    }



char CompetitionsMenuPage::getReturnToMenu() {
    return mReturnToMenu;
    }



char CompetitionsMenuPage::getStartCompetition() {
    return mStartCompetition;
    }



int CompetitionsMenuPage::getCompetitionID() {
    HouseRecord *record = mPickList.getSelectedHouse();

    if( record == NULL ) {
        return -1;
        }
    else {
        return record->uniqueID;
        }
    }



void CompetitionsMenuPage::actionPerformed( GUIComponent *inTarget ) {
    if( inTarget == &mMenuButton ) {
        mReturnToMenu = true;
        }
    else if( inTarget == &mPickList ) {
        if( mPickList.getSelectedHouse() == NULL ) {
            mStartCompetitionButton.setVisible( false );
            }
        else {
            mStartCompetitionButton.setVisible( true );
            }
        }
    else if( inTarget == &mStartCompetitionButton ) {
        mStartCompetition = true;
        }
    
    }



void CompetitionsMenuPage::step() {
    }


        
void CompetitionsMenuPage::draw( doublePair inViewCenter, 
                          double inViewSize ) {
    }


        
void CompetitionsMenuPage::makeActive( char inFresh ) {
    if( !inFresh ) {
        return;
        }
    
    mPickList.refreshList( true, true );
    
    mReturnToMenu = false;
    mStartCompetition = false;

    mStatusMessageKey = NULL;
    mStatusError = false;
    }
