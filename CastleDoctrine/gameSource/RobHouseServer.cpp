#include "RobHouseServer.h"

#include "minorGems/game/game.h"

#include "minorGems/util/stringUtils.h"



#include "houseObjects.h"
#include "houseTransitions.h"
#include "tools.h"

#include <math.h>
#include <limits.h>



RobHouseServer::RobHouseServer() : RobHouseGridDisplay(0,0),
        mClientHouseMapIDs( NULL ),
        mClientHouseMapCellStates( NULL ),
        mClientHouseMapMobileIDs( NULL ),
        mClientHouseMapMobileCellStates( NULL ),
        mTileWasRevealedMap( NULL ),
        mClientDead( false ),
        mBackpackSlots() {}

RobHouseServer::~RobHouseServer() {
    if ( mClientHouseMapIDs != NULL ) {
        delete [] mClientHouseMapIDs;
        }
    if ( mClientHouseMapCellStates != NULL ) {
        delete [] mClientHouseMapCellStates;
        }
    if ( mClientHouseMapMobileIDs != NULL ) {
        delete [] mClientHouseMapMobileIDs;
        }
    if ( mClientHouseMapMobileCellStates != NULL ) {
        delete [] mClientHouseMapMobileCellStates;
        }
    if ( mTileWasRevealedMap != NULL ) {
        delete [] mTileWasRevealedMap;
        }
    }

void RobHouseServer::setHouseMap( const char *inHouseMap ) {
    RobHouseGridDisplay::setHouseMap( inHouseMap );

    mClientHouseMapIDs = new int[ mNumMapSpots ];
    mClientHouseMapCellStates = new int[ mNumMapSpots ];
    mClientHouseMapMobileIDs = new int[ mNumMapSpots ];
    mClientHouseMapMobileCellStates = new int[ mNumMapSpots ];
    mTileWasRevealedMap = new char[ mNumMapSpots ];

    memset( mTileWasRevealedMap, false, mNumMapSpots );

    for( int i=0; i<mNumMapSpots; i++ ) {
        // client starts with an unseen empty house.
        mClientHouseMapIDs[i] = 800;
        mClientHouseMapCellStates[i] = 1;
        mClientHouseMapMobileIDs[i] = 0;
        mClientHouseMapMobileCellStates[i] = 0;
        mTileWasRevealedMap[i] = 0;
        }
    }

void RobHouseServer::setBackpackSlots( SimpleVector<QuantityRecord> backpackSlots ) {
    mBackpackSlots = backpackSlots;
    }
SimpleVector<QuantityRecord> RobHouseServer::getBackpackSlots() {
    return mBackpackSlots;
    }

char *RobHouseServer::handleMove( const char *move ) {
    int tool,idx;
    char toolAllowed;
    switch (*move) {
        case 'm': 
            sscanf( move, "m%d", &idx );
            moveRobber( idx );
            break;
        case 't':
            sscanf( move, "t%d@%d", &tool, &idx );
            toolAllowed = subtractFromQuantity( &mBackpackSlots, tool );
            if ( toolAllowed ) {
                startUsingTool( tool );
                applyCurrentTool( idx );
                }
            else {
                mForbiddenMoveHappened = true;
                }
            break;
        case 'L':
            if( mRobberIndex == mStartIndex )
                robberTriedToLeave();
            break;
        case 'l':
            break;
        }

    static char *cmds[ HOUSE_D*HOUSE_D*2 ];
    int cmdn = 0;
    for( int i=0; i<HOUSE_D*HOUSE_D; i++ ) {
        int fullI = subToFull( i );
        if ( mTileRevealedMap[ i ] || mTileWasRevealedMap[ fullI ] ) {
            if ( mClientHouseMapIDs[ fullI ] != mHouseMapIDs[ fullI ]
                    || mClientHouseMapCellStates[ fullI ] != mHouseMapCellStates[ fullI ]) {
                cmds[ cmdn++ ] = autoSprintf("t:%d:%d:%d",
                        fullI, mHouseMapIDs[ fullI ], mHouseMapCellStates[ fullI ]);
                mClientHouseMapIDs[ fullI ] = mHouseMapIDs[ fullI ];
                mClientHouseMapCellStates[ fullI ] = mHouseMapCellStates[ fullI ];
                }
            if ( mClientHouseMapMobileIDs[ fullI ] != mHouseMapMobileIDs[ fullI ]
                    || mClientHouseMapMobileCellStates[ fullI ] != mHouseMapMobileCellStates[ fullI ]) {
                cmds[ cmdn++ ] = autoSprintf("m:%d:%d:%d",
                        fullI, mHouseMapMobileIDs[ fullI ], mHouseMapMobileCellStates[ fullI ]);
                mClientHouseMapMobileIDs[ fullI ] = mHouseMapMobileIDs[ fullI ];
                mClientHouseMapMobileCellStates[ fullI ] = mHouseMapMobileCellStates[ fullI ];
                }
            }
        mTileWasRevealedMap[ fullI ] = mTileRevealedMap[ i ];
        }
    if ( mDead && ! mClientDead ) {
        cmds[ cmdn++ ] = autoSprintf("D:%d:%d:%d", mDeathSourceID, mDeathSourceState, mRobberState);
        mClientDead = true;
    }
    char* ret;
    if ( cmdn == 0 ) {
        ret = stringDuplicate( "" );
        }
    else {
        ret = join(cmds, cmdn, "#");
        for (int i=0; i<cmdn; i++)
            delete [] cmds[i];
        }
    return ret;
    }
