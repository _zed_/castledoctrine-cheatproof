#include "RobHousePage.h"

#include "message.h"
#include "balance.h"

#include "tools.h"

#include "seededMusic.h"
#include "musicPlayer.h"

#include "minorGems/game/Font.h"
#include "minorGems/game/game.h"
#include "minorGems/game/drawUtils.h"

#include "minorGems/util/stringUtils.h"
#include "minorGems/util/SettingsManager.h"




extern Font *mainFont;


extern char *serverURL;

extern int userID;

extern int musicOff;


RobHousePage::RobHousePage( int inNumToolSlots )
        : mGridDisplay( 0, 0 ),
          mBlueprintButton( mainFont, -8, -1.5, 
                            translate( "viewBlueprintShort" ) ),
          mUndoButton( mainFont, 7, -6,
                            translate( "robUndoShort" ) ),
          mUndoManyButton( mainFont, 9, -6,
                            "x10" ),
          mDoneButton( mainFont, 8, -4.5, translate( "flee" ) ),
          mSuicideConfirmCheckbox( 8, -3.625, 1/16.0 ),
          mMusicToggleButton( "musicOn.tga", "musicOff.tga", -8, -6, 1/16.0 ),
          mSafeMoveToggleButton( "safeMoveOff.tga", "safeMoveOn.tga", 
                                 8, -2.5, 1/16.0 ),
          mGallery( mainFont, -8, -1 ),
          mToolPicker( -8, 5.75, true ),
          mMusicSeed( 0 ),
          mShowTimeAtEnd( false ),
          mCompetitionID( 0 ),
          mStepCost( 0 ),
          mInitCash( 0 ),
          mToolCosts( 0 ),
          mNumToolSlots( 0 ),
          mDescription( NULL ),
          mBlueprints( NULL ),
          mDeathMessage( NULL ) {    

    addComponent( &mBlueprintButton );
    addComponent( &mUndoButton );
    addComponent( &mUndoManyButton );
    addComponent( &mDoneButton );
    addComponent( &mSuicideConfirmCheckbox );
    addComponent( &mGallery );
    addComponent( &mGridDisplay );
    addComponent( &mMusicToggleButton );
    addComponent( &mSafeMoveToggleButton );

    mGallery.setAllowEdit( false );
    

    mBlueprintButton.setMouseOverTip( translate( "viewBlueprintTip" ) );
    mUndoButton.setMouseOverTip( translate( "robUndoTip" ) );
    mUndoManyButton.setMouseOverTip( translate( "robUndoManyTip" ) );

    mDoneButton.setMouseOverTip( translate( "unconfirmedFleeTip" ) );
    mSuicideConfirmCheckbox.setMouseOverTip( 
        translate( "fleeConfirmTip" ) );
    mSuicideConfirmCheckbox.setMouseOverTipB( 
        translate( "fleeConfirmTip" ) );
    
    mBlueprintButton.addActionListener( this );
    mBlueprintButton.setVisible( false );
    mUndoButton.addActionListener( this );
    mUndoManyButton.addActionListener( this );
    mDoneButton.addActionListener( this );
    mSuicideConfirmCheckbox.addActionListener( this );
    mGridDisplay.addActionListener( this );
    mMusicToggleButton.addActionListener( this );
    mSafeMoveToggleButton.addActionListener( this );

    mMusicToggleButton.setMouseOverTip( translate( "musicOff" ) );
    mMusicToggleButton.setMouseOverTipB( translate( "musicOn" ) );


    mSafeMoveToggleButton.setMouseOverTip( translate( "safeMoveOn" ) );
    mSafeMoveToggleButton.setMouseOverTipB( translate( "safeMoveOff" ) );
    
    initToolSlots( inNumToolSlots );
    }

void RobHousePage::initToolSlots( int inNumToolSlots ) {
    mNumToolSlots = inNumToolSlots;

    if( inNumToolSlots <= 0 ) {
        return;
        }

    const int columns = ( mNumToolSlots + 5 ) / 6;
    const int rows = ( mNumToolSlots + columns - 1 ) / columns;
    const float verticalSeparation = 6.0 / rows < 1.3 ? 1.3 : 6.0 / rows;
    const float horizontalSeparation = 3.0 / columns < 1.2 ? 1.2 : 3.0 / columns;
    const float verticalStart = (rows - 1) * verticalSeparation - 0.5;
    const float horizontalStart = 8.0 - (columns - 1) * horizontalSeparation / 2.0;
    doublePair slotCenter = { horizontalStart, verticalStart };

    int currentSlot = 0;
    for( int c=0; c<columns; c++ ) {
        
        for( int i=0; i < rows; i++ ) {
            
            mToolSlots[currentSlot] =
                new InventorySlotButton( mainFont, 
                                         slotCenter.x, slotCenter.y,
                                         1 / 32.0 );
            slotCenter.y -= verticalSeparation;
            
            addComponent( mToolSlots[currentSlot] );
            mToolSlots[currentSlot]->addActionListener( this );
            currentSlot++;
            }
        
        // next column
        slotCenter.y = verticalStart;
        slotCenter.x += horizontalSeparation;
        }
    }

RobHousePage::~RobHousePage() {
    
    if( mDescription != NULL ) {
        delete [] mDescription;
        }
    if( mBlueprints != NULL ) {
        delete [] mBlueprints;
        }
    if( mDeathMessage != NULL ) {
        delete [] mDeathMessage;
        }
    

    for( int i=0; i<mNumToolSlots; i++ ) {
        delete mToolSlots[i];
        }
    }




void RobHousePage::setWifeName( const char *inWifeName ) {
    mGridDisplay.setWifeName( inWifeName );
    }

void RobHousePage::setSonName( const char *inSonName ) {
    mGridDisplay.setSonName( inSonName );
    }

void RobHousePage::setDaughterName( const char *inDaughterName ) {
    mGridDisplay.setDaughterName( inDaughterName );
    }



void RobHousePage::setHouseMap( char *inHouseMap ) {
    if( mBlueprints != NULL ) {
        delete [] mBlueprints;
        }
    mBlueprints = stringDuplicate( inHouseMap );

    mGridDisplay.setHouseMap( inHouseMap );
    
    mGallery.instantFadeOut( mGridDisplay.getAboutToLeave() );

    if( mDeathMessage != NULL ) {
        delete [] mDeathMessage;
        mDeathMessage = NULL;
        }

    // back to default button text
    mDoneButton.setLabelText( translate( "flee" ) );
    mDoneButton.setMouseOverTip( translate( "unconfirmedFleeTip" ) );
    mSuicideConfirmCheckbox.setVisible( true );
    mSuicideConfirmCheckbox.setToggled( false );
    }



char *RobHousePage::getHouseMap() {
    return mGridDisplay.getHouseMap();
    }

void RobHousePage::jumpToMoveList( char *inMoveList ) {
    char* baseMap = getBlueprintMap();
    if( ! baseMap ) {
        return;
        }
    setHouseMap( baseMap );
    delete [] baseMap;

    if( mCompetitionID > 0 ) {
        char *priceList = stringDuplicate( mPriceList );
        initialiseCompetition( mCompetitionID, mInitCash, mStepCost,
                priceList );
        delete [] priceList;
        }

    mGridDisplay.jumpToMoveList( inMoveList );

    int numMoves;
    char **moves = split( inMoveList, "#", &numMoves );

    for( int i=0; i<numMoves; i++ ) {
        const char *move = moves[i];
        if( strlen( move ) > 0 ) {
            if( move[0] == 't' ) {
                int toolID, targetIndex;

                sscanf( move, "t%d@%d", &toolID, &targetIndex );

                for( int j=0; j<mNumToolSlots; j++ ) {
                    if( mToolSlots[j]->getObject() == toolID ) {
                        mToolSlots[j]->addToQuantity( -1 );
                        break;
                        }
                    }

                if( mCompetitionID > 0 ) {
                    mToolCosts += mToolPicker.getPrice( toolID );
                    }
                }
            }
        }
    delete [] moves;

    mGridDisplay.stopUsingTool();
    for( int j=0; j<mNumToolSlots; j++ ) {
        if( mToolSlots[j]->getRingOn() ) {
            mToolSlots[j]->setRingOn( false );
            break;
            }
        }
    }

void RobHousePage::undo( int undoMoves ) {
    char *moveList = mGridDisplay.getMoveList( true );

    int numMoves = 0;
    char **moves = split( moveList, "#", &numMoves );
    delete [] moveList;

    char *newMoveList = NULL;
    if( numMoves <= undoMoves ) {
        newMoveList = stringDuplicate( "#" );
        }
    else {
        newMoveList = join( moves, numMoves - undoMoves, "#" );
        }
    delete [] moves;

    jumpToMoveList( newMoveList );

    delete [] newMoveList;
    }

void RobHousePage::setSynchSocket( Socket *synchSock ) {
    mGridDisplay.setSynchSocket( synchSock );
}

Socket *RobHousePage::getSynchSocket() {
    return mGridDisplay.getSynchSocket();
}


char *RobHousePage::getBlueprintMap() {
    if( mBlueprints == NULL ) {
        return NULL;
        }
    return stringDuplicate( mBlueprints );
    }



int RobHousePage::getVisibleOffsetX() {
    return mGridDisplay.getVisibleOffsetX();
    }



int RobHousePage::getVisibleOffsetY() {
    return mGridDisplay.getVisibleOffsetY();
    }



void RobHousePage::setBackpackContents( char *inBackpackContents ) {

    inventorySlotsFromString( inBackpackContents,
                              mToolSlots, NUM_PACK_SLOTS );
    }



char *RobHousePage::getBackpackContents() {

    return stringFromInventorySlots( mToolSlots, mNumToolSlots );
    }

void RobHousePage::setGalleryContents( char *inGalleryContents ) {

    mGallery.setGalleryContents( inGalleryContents );
    }


void RobHousePage::setWifeMoney( int inMoney ) {
    mGridDisplay.setWifeMoney( inMoney );
    }


void RobHousePage::setMusicSeed( int inMusicSeed ) {
    mMusicSeed = inMusicSeed;
    }


void RobHousePage::setMaxSeconds( int inMaxSeconds ) {
    if( inMaxSeconds >= 0 ) {
        mEndTime = game_time(NULL) + inMaxSeconds;
        mTimeMessageFade = 0.0f;
        mTimeMessageFadeDirection = 1.0f;
        mShowTimeAtEnd = true;
        }
    else {
        // no time-limit
        mShowTimeAtEnd = false;
        }
    }


void RobHousePage::setDescription( const char *inDescription ) {
    if( mDescription != NULL ) {
        delete [] mDescription;
        }
    mDescription = stringDuplicate( inDescription );
    }

void RobHousePage::initialiseCompetition( int inCompetitionID,
        int inInitCash, int inStepCost, char *inPriceList ) {
    mCompetitionID = inCompetitionID;

    mBlueprintButton.setVisible( true );

    mInitCash = inInitCash;
    mStepCost = inStepCost;

    mToolCosts = 0;

    if( inPriceList == NULL) {
        return;
        }

    if( mPriceList != NULL ) {
        delete [] mPriceList;
        }
    mPriceList = stringDuplicate( inPriceList );

    mToolPicker.setPriceList( inPriceList );

    int numTools = 0;
    ObjectPriceRecord *toolRecords = mToolPicker.getPrices( &numTools );
    numTools = numTools <= mNumToolSlots ? numTools : mNumToolSlots;

    for( int i=0; i<numTools; i++ ) {
        mToolSlots[i]->setObject( toolRecords[i].id );
        mToolSlots[i]->setQuantity( 999 );
        }

    delete [] toolRecords;
    }

char *RobHousePage::getPriceList() {
    if( mPriceList == NULL ) {
        return NULL;
        }
    else {
        return stringDuplicate( mPriceList );
        }
}

void RobHousePage::actionPerformed( GUIComponent *inTarget ) {
    if( inTarget == &mSuicideConfirmCheckbox ) {
        if( mSuicideConfirmCheckbox.getToggled() ) {
            mDoneButton.setMouseOverTip( 
                translate( "fleeTip" ) );
            }
        else {
            mDoneButton.setMouseOverTip( 
                translate( "unconfirmedFleeTip" ) );
            }    
        }
    else if( inTarget == &mBlueprintButton ) {
        mViewBlueprint = true;
        }
    else if( inTarget == &mUndoButton ) {
        undo( 1 );
        }
    else if( inTarget == &mUndoManyButton ) {
        undo( 10 );
        }
    else if( inTarget == &mDoneButton ) {
        if( mSuicideConfirmCheckbox.isVisible() && 
            ! mSuicideConfirmCheckbox.getToggled() ) {
            return;
            }
        if( mSuicideConfirmCheckbox.isVisible() ) {
            // commited suicide to end robbery
            // in that case, mGridDisplay does not detect the suicide
            // must force family end process
            mGridDisplay.processFamilyAndMobilesAtEnd();
            }

        mDone = true;
        clearNotes();
        }
    else if( inTarget == &mMusicToggleButton ) {
        musicOff = mMusicToggleButton.getToggled();
        
        if( musicOff ) {
            setMusicLoudness( 0 );
            }
        else {
            setMusicLoudness( 1 );
            }
        SettingsManager::setSetting( "musicOff", musicOff );
        }
    else if( inTarget == &mSafeMoveToggleButton ) {
        char on = mSafeMoveToggleButton.getToggled();
        
        mGridDisplay.setSafeMoveMode( on );
        }
    else if( inTarget == &mGridDisplay ) {
        mGallery.fadeOut( mGridDisplay.getAboutToLeave() );

        // activity on house map
            
        if( mGridDisplay.getToolJustUsed() ) {    
            // spend tool from active slot in backpack
            for( int j=0; j<mNumToolSlots; j++ ) {
                if( mToolSlots[j]->getRingOn() ) {

                    if( mCompetitionID > 0 ) {
                        int object = mToolSlots[j]->getObject();
                        mToolCosts += mToolPicker.getPrice( object );
                    }
        
                    mToolSlots[j]->addToQuantity( -1 );
                    mToolSlots[j]->setRingOn( false );

                    break;
                    }
                }
            }
        else {
            // auto turn-off active slot
            // player moved without using it
            for( int j=0; j<mNumToolSlots; j++ ) {
                if( mToolSlots[j]->getRingOn() ) {
                    mToolSlots[j]->setRingOn( false );
                    break;
                    }
                }
            }

        actionHappened();

        
        if( mGridDisplay.getSuccess() ) {
            mDone = true;
            clearNotes();
            }
        else if( mGridDisplay.getDead() ) {
            if( mDeathMessage != NULL ) {
                delete [] mDeathMessage;
                }
            
            char *wifeName = mGridDisplay.getWifeName();
            
            char *objectDescription =
                getObjectDescription( 
                        mGridDisplay.getDeathSourceID(),
                        mGridDisplay.getDeathSourceState(),
                        wifeName );

            if( isPropertySet( mGridDisplay.getDeathSourceID(),
                               mGridDisplay.getDeathSourceState(),
                               wife ) ) {
                char *newDescription = autoSprintf( objectDescription,
                                                    wifeName );
                delete [] objectDescription;
                objectDescription = newDescription;
                }
            else if( isPropertySet( mGridDisplay.getDeathSourceID(),
                                    mGridDisplay.getDeathSourceState(),
                                    son ) ) {
                char *sonName = mGridDisplay.getSonName();
                char *newDescription = autoSprintf( objectDescription,
                                                    sonName );
                delete [] sonName;
                delete [] objectDescription;
                objectDescription = newDescription;
                }
            else if( isPropertySet( mGridDisplay.getDeathSourceID(),
                                    mGridDisplay.getDeathSourceState(),
                                    daughter ) ) {
                char *daughterName = mGridDisplay.getDaughterName();
                char *newDescription = autoSprintf( objectDescription,
                                                    daughterName );
                delete [] daughterName;
                delete [] objectDescription;
                objectDescription = newDescription;
                }
            
            delete [] wifeName;
            
            mDeathMessage = 
                autoSprintf( 
                    "%s  %s",
                    translate( "killedBy" ),
                    objectDescription );
            
            delete [] objectDescription;
            
            mDoneButton.setLabelText( translate( "doneRobDead" ) );
            mDoneButton.setMouseOverTip( "" );
            mSuicideConfirmCheckbox.setVisible( false );
            mBlueprintButton.setVisible( false );
            }
        }
    else if( ! mGridDisplay.getDead() ) {
        // check backpack slots
        for( int i=0; i<mNumToolSlots; i++ ) {
            if( inTarget == mToolSlots[i] ) {

                mGridDisplay.stopUsingTool();

                char oldOn = mToolSlots[i]->getRingOn();
                
                // turn all other slots off first (only one ring at a time)
                for( int j=0; j<mNumToolSlots; j++ ) {
                    mToolSlots[j]->setRingOn( false );
                    }
                
                if( mToolSlots[i]->getObject() != -1 ) {
                    mToolSlots[i]->setRingOn( !oldOn );

                    if( !oldOn ) {
                        mGridDisplay.startUsingTool( 
                            mToolSlots[i]->getObject() );
                        }
                    }
                else {
                    mToolSlots[i]->setRingOn( false );
                    printf("wtf\n");
                    }
                
                break;
                }
            }
        
        
        }
    }


        
void RobHousePage::makeActive( char inFresh ) {
    LiveHousePage::makeActive( inFresh );

    char isCompetition = (mCompetitionID > 0);

    LiveHousePage::setPingsRequired( ! isCompetition );

    mViewBlueprint = false;
    
    if( !inFresh ) {
        return;
        }
    
    mDone = false;

    mBlueprintButton.setVisible( isCompetition );

    mUndoButton.setVisible( getSynchSocket() == NULL );
    mUndoManyButton.setVisible( getSynchSocket() == NULL );
    
    // no tool tip
    setToolTip( NULL );



    
    setMusicFromSeed( mMusicSeed );

    mMusicToggleButton.setToggled( musicOff );
    

    mSafeMoveToggleButton.setToggled( false );
    mGridDisplay.setSafeMoveMode( false );
    }




void RobHousePage::step() {
    LiveHousePage::step();

    mGridDisplay.synchReceive( true );

    mGridDisplay.setLeaveCanBeShown( ! mGallery.isVisible() );
    }



static int displayTimeMessageBelow = 60;


char RobHousePage::canShowWaitingIcon() {
    if( ! mShowTimeAtEnd ) {
        return true;
        }
    
    int timeLeft = mEndTime - game_time( NULL );

    if( timeLeft < displayTimeMessageBelow ) {
        return false;
        }

    return true;
    }



extern double frameRateFactor;


void RobHousePage::draw( doublePair inViewCenter, 
                         double inViewSize ) {
     
    if( mDescription != NULL ) {
        doublePair labelPos = { 0, 6.75 };
        
        if( strstr( mDescription, "##" ) != NULL ) {
            // two lines, move up a bit
            labelPos.y = 7;
            }

        drawMessage( mDescription, labelPos );
        }

    if( mDeathMessage != NULL ) {
        doublePair labelPos = { 0, -6.25 };
        
        drawMessage( mDeathMessage, labelPos, true );
        }

    
    if( mNumToolSlots > 0 && mNumToolSlots <= 8 ) {
        doublePair labelPos = { 8, 5.5 };
        drawMessage( "robBackpack", labelPos );
        }



    if( !mShowTimeAtEnd ) {
        return;
        }

    int timeLeft = mEndTime - game_time( NULL );
    
    if( timeLeft < 0 ) {
        timeLeft = 0;
        }

    if( timeLeft < displayTimeMessageBelow ) {

        if( timeLeft > 10 ) {
            if( mTimeMessageFade < 1 ) {
                // fade in the first time
                mTimeMessageFade += 
                    mTimeMessageFadeDirection * 0.0166 * frameRateFactor;
                if( mTimeMessageFade > 1 ) {
                    mTimeMessageFadeDirection = -1;
                    mTimeMessageFade = 1;
                    }
                }
            }
        else {
            mTimeMessageFade += 
                mTimeMessageFadeDirection * 0.0166 * frameRateFactor;
        
            if( mTimeMessageFade < 0.5 && mTimeMessageFadeDirection < 0 ) {
                mTimeMessageFadeDirection = 1;
                mTimeMessageFade = 0.5;
                }
            else if( mTimeMessageFade > 1 ) {
                mTimeMessageFadeDirection = -1;
                mTimeMessageFade = 1;
                }
            }
        

        doublePair labelPos = { 8, 7 };

        const char *key = "robCops";
        if( mCompetitionID > 0 ) {
            key = "compEnding";
            }

        drawMessage( key, labelPos, true, mTimeMessageFade );

        labelPos.y -= 0.75;
        labelPos.x -= 0.5;
        char *timeString = autoSprintf( "0:%02d", timeLeft );
        
        setDrawColor( 1, 0, 0, mTimeMessageFade );

        mainFont->drawString( timeString, 
                              labelPos, alignLeft );

        delete [] timeString;
        }
    }

void RobHousePage::drawUnderComponents( doublePair inViewCenter, 
                                         double inViewSize ) {
    if( mCompetitionID > 0 ) {
        int costs = mToolCosts + mStepCost * mGridDisplay.getStepsTaken();
        drawBalance( mInitCash, costs, false, "competitionBalance", -16, -5 );
        }
    }

        

