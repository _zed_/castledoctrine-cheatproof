#include "robberySimulator.h"
#include "ReplayRobHouseGridDisplay.h"
#include "RobHouseServer.h"
#include "readLine.h"

#include "minorGems/util/stringUtils.h"


char *simulateRobbery( const char *houseMap,
    SimpleVector<QuantityRecord> backpackSlots,
    int wifeMoney,
    char *moveList ) {

    ReplayRobHouseGridDisplay *replayCheckerDisplay =
        new ReplayRobHouseGridDisplay( 0, 0 );

    replayCheckerDisplay->setHouseMap( houseMap );

    replayCheckerDisplay->setMoveList( moveList );

    replayCheckerDisplay->setWifeMoney( wifeMoney );
    
    replayCheckerDisplay->setWifeName( "SimWife" );
    replayCheckerDisplay->setSonName( "SimSon" );
    replayCheckerDisplay->setDaughterName( "SimDaughter" );

    replayCheckerDisplay->playAtFullSpeed();
    

    char moveListIncorrect = false;
    
    while( !replayCheckerDisplay->getDead() && 
           replayCheckerDisplay->getSuccess() == 0 &&
           ! replayCheckerDisplay->getMoveListExhausted() ) {
        
        replayCheckerDisplay->step();

        if( replayCheckerDisplay->didForbiddenMoveHappen() ) {
            moveListIncorrect = true;
            break;
            }

        if( replayCheckerDisplay->getToolJustUsed() ) {
            
            int pickedID = replayCheckerDisplay->getToolIDJustPicked();
            
            if( pickedID != -1 ) {
                char toolAllowed = subtractFromQuantity( &backpackSlots,
                                                         pickedID );

                if( !toolAllowed ) {
                    moveListIncorrect = true;
                    break;
                    }
                }
            }
        }
    

    if( !replayCheckerDisplay->getDead() && 
        replayCheckerDisplay->getSuccess() == 0 ) {
        // move list ran out or was incorrect before robbery ended properly!
        
        delete replayCheckerDisplay;

        return stringDuplicate( 
            "FAILED : move list ended before robbery ended" );
        }
    
    if( replayCheckerDisplay->getDead() && 
        replayCheckerDisplay->getDeathSourceID() == -1 ) {
        // dead, but not killed by anything
        // suicide.

        replayCheckerDisplay->processFamilyAndMobilesAtEnd();
        }

    
    /*
Returns:
success
wife_killed
wife_robbed
family_killed_count
end_backpack_contents
end_house_map
    */
    
    char *response;

    if( !moveListIncorrect ) {
        
        char *endBackpack = toString( &backpackSlots );


        char *endHouseMap = replayCheckerDisplay->getHouseMap();
        
        response = 
            autoSprintf( "%d\n"
                         "%d\n"
                         "%d\n"
                         "%d\n"
                         "%d\n"
                         "%s\n"
                         "%s",
                         replayCheckerDisplay->getSuccess(),
                         replayCheckerDisplay->getWifeKilledRobber(),
                         replayCheckerDisplay->getWifeKilled(),
                         replayCheckerDisplay->getWifeRobbed(),
                         replayCheckerDisplay->getFamilyKilledCount(),
                         endBackpack,
                         endHouseMap );


        delete [] endBackpack;
        delete [] endHouseMap;
        }
    else {
        response = stringDuplicate( 
            "FAILED : move list contained a forbidden move of tool use" );
        }

    delete replayCheckerDisplay;

    return response;
    }


RobResult *doRob( const RobData *data, Socket *sock ) {
    RobHouseServer *robDisplay = new RobHouseServer();

    robDisplay->setHouseMap( data->houseMap );

    robDisplay->setBackpackSlots( data->backpackSlots );

    robDisplay->setWifeMoney( data->wifeMoney );

    robDisplay->setWifeName( "SimWife" );
    robDisplay->setSonName( "SimSon" );
    robDisplay->setDaughterName( "SimDaughter" );

    sock->send( (unsigned char *) "SYNCH\n", strlen("SYNCH\n") );

    printf( "Beginning synchronous robbery.\n" );

    while (true) {
        char *move = readLine( sock );
        if (!move) {
            // Client has closed the socket (or the connection has otherwise
            // failed) - treat this as the client declaring the robbery to be
            // over.
            break;
        }

        printf( "<< %s\n", move );
        if ( strcmp(move, "." ) == 0 ) {
            break;
            }
        char *resp = robDisplay->handleMove(move);
        printf( ">> %s\n", resp );

        sock->send( (unsigned char *) resp,strlen(resp));
        sock->send( (unsigned char *)"\n", strlen( "\n" ));

        delete [] resp;
        delete [] move;
        }

    printf( "Synchronous robbery ends with result %d\n", robDisplay->getSuccess() );

    SimpleVector<QuantityRecord> endBackpackSlots = robDisplay->getBackpackSlots();
    char *endBackpack = toString( &endBackpackSlots );
    char *moveList = robDisplay->getMoveList();
    char *endHouseMap = robDisplay->getHouseMap();

    RobResult *result = new RobResult(robDisplay->getSuccess(),
        robDisplay->getWifeKilledRobber(),
        robDisplay->getWifeKilled(),
        robDisplay->getWifeRobbed(),
        robDisplay->getFamilyKilledCount(),
        endBackpack,
        endHouseMap,
        moveList);

    delete moveList;
    delete endBackpack;
    delete endHouseMap;

    delete robDisplay;

    return result;
    }
