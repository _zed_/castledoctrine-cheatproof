#include "GamePage.h"

#include "TextField.h"
#include "TextButton.h"
#include "CheckboxButton.h"

#include "minorGems/ui/event/ActionListener.h"


class MapStoragePage : public GamePage, public ActionListener
{
        
    public:
        
        MapStoragePage();
        
        virtual ~MapStoragePage();

        virtual char getReturnToMenu();
    
        virtual void actionPerformed( GUIComponent *inTarget );
    
        virtual void draw( doublePair inViewCenter, 
                           double inViewSize );
        
        virtual void makeActive( char inFresh );
    
        char getStoreUserMap();
        char getRestoreUserMap();
        char getShowUserMap();
        char getShareMap();
    
        int getMapNumber();
    
    protected:
    
        static const int NUM_USER_MAPS_MAX = 3;
    
        TextButton mMenuButton;
    
        CheckboxButton mStoreRestoreConfirmCheckbox;
    
        char mReturnToMenu;
        char mStoreUserMap;
        char mRestoreUserMap;
        char mShowUserMap;
        char mShareMap;
    
        int mMapNumber;
        
        TextButton mShareMapButton;
    
        TextButton* mStoreUserMapButton[ NUM_USER_MAPS_MAX ];
        TextButton* mRestoreUserMapButton[ NUM_USER_MAPS_MAX ];
        TextButton* mShowUserMapButton[ NUM_USER_MAPS_MAX ];
};
