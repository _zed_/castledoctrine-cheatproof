#include "RobCheckoutHousePage.h"
#include "ticketHash.h"
#include "message.h"
#include "nameProcessing.h"
#include "LiveHousePage.h"

#include "serialWebRequests.h"

#include "sha1Encryption.h"

#include "secureString.h"

#include "mapEncryptionKey.h"

#include "readLine.h"

#include "minorGems/game/Font.h"
#include "minorGems/game/game.h"

#include "minorGems/util/stringUtils.h"
#include "minorGems/crypto/hashes/sha1.h"
#include "minorGems/network/SocketClient.h"


extern Font *mainFont;


extern char *serverURL;

extern int userID;



RobCheckoutHousePage::RobCheckoutHousePage() 
        : mWebRequest( -1 ),
          mMapEncryptionKey( NULL ),
          mWifeName( NULL ),
          mSonName( NULL ),
          mDaughterName( NULL ),
          mOwnerName( NULL ),
          mHouseMap( NULL ),
          mBackpackContents( NULL ),
          mGalleryContents( NULL ),
          mSynchSocket( NULL ),
          mMenuButton( mainFont, 4, -4, translate( "returnMenu" ) ),
          mReturnToMenu( false ),
          mToRobCharacterName( NULL ) {

    addComponent( &mMenuButton );
    mMenuButton.addActionListener( this );
    }


void RobCheckoutHousePage::clearDataMembers() {
    if( mWifeName != NULL ) {
        delete [] mWifeName;
        }
    mWifeName = NULL;
    
    if( mSonName != NULL ) {
        delete [] mSonName;
        }
    mSonName = NULL;
    
    if( mDaughterName != NULL ) {
        delete [] mDaughterName;
        }
    mDaughterName = NULL;
    
    if( mOwnerName != NULL ) {
        delete [] mOwnerName;
        }
    mOwnerName = NULL;
    
    if( mHouseMap != NULL ) {
        clearString( mHouseMap );
        }
    mHouseMap = NULL;
    
    if( mBackpackContents != NULL ) {
        delete [] mBackpackContents;
        }
    mBackpackContents = NULL;
    
    if( mGalleryContents != NULL ) {
        delete [] mGalleryContents;
        }
    mGalleryContents = NULL;

    mSynchSocket = NULL;
    }


        
RobCheckoutHousePage::~RobCheckoutHousePage() {
    if( mWebRequest != -1 ) {
        clearWebRequestSerial( mWebRequest );
        }
    
    if( mMapEncryptionKey != NULL ) {
        delete [] mMapEncryptionKey;
        }

    clearDataMembers();

    if( mToRobCharacterName != NULL ) {
        delete [] mToRobCharacterName;
        }
    mToRobCharacterName = NULL;
    }



void RobCheckoutHousePage::setToRobHomeID( int inID ) {
    mToRobHomeID = inID;
    }


void RobCheckoutHousePage::setToRobCharacterName( const char *inName ) {
    if( mToRobCharacterName != NULL ) {
        delete [] mToRobCharacterName;
        }
    mToRobCharacterName = stringDuplicate( inName );
    }



char RobCheckoutHousePage::getReturnToMenu() {
    return mReturnToMenu;
    }




char *RobCheckoutHousePage::getWifeName() {
    if( mWifeName == NULL ) {
        return NULL;
        }
    else {
        return stringDuplicate( mWifeName );
        }
    }


char *RobCheckoutHousePage::getSonName() {
    if( mSonName == NULL ) {
        return NULL;
        }
    else {
        return stringDuplicate( mSonName );
        }
    }


char *RobCheckoutHousePage::getDaughterName() {
    if( mDaughterName == NULL ) {
        return NULL;
        }
    else {
        return stringDuplicate( mDaughterName );
        }
    }



char *RobCheckoutHousePage::getHouseMap() {
    if( mHouseMap == NULL ) {
        return NULL;
        }
    else {
        char *returnValue = stringDuplicate( mHouseMap );
        clearString( mHouseMap );
        mHouseMap = NULL;
        
        return returnValue;
        }
    }


char *RobCheckoutHousePage::getOwnerName() {
    if( mOwnerName == NULL ) {
        return NULL;
        }
    else {
        return stringDuplicate( mOwnerName );
        }
    }



char *RobCheckoutHousePage::getBackpackContents() {
    if( mBackpackContents == NULL ) {
        return NULL;
        }
    else {
        return stringDuplicate( mBackpackContents );
        }
    }



char *RobCheckoutHousePage::getGalleryContents() {
    if( mGalleryContents == NULL ) {
        return NULL;
        }
    else {
        return stringDuplicate( mGalleryContents );
        }
    }


int RobCheckoutHousePage::getWifeMoney() {
    return mWifeMoney;
    }


int RobCheckoutHousePage::getMusicSeed() {
    return mMusicSeed;
    }


int RobCheckoutHousePage::getMaxSeconds() {
    return mMaxSeconds;
    }

Socket *RobCheckoutHousePage::getSynchSocket() {
    return mSynchSocket;
}


void RobCheckoutHousePage::actionPerformed( GUIComponent *inTarget ) {
    if( inTarget == &mMenuButton ) {
        mReturnToMenu = true;
        }
    }


void RobCheckoutHousePage::step() {
    if( mWebRequest != -1 ) {
            
        int stepResult = stepWebRequestSerial( mWebRequest );
        
        if( stepResult != 0 ) {
            setWaiting( false );
            }

        switch( stepResult ) {
            case 0:
                break;
            case -1:
                mStatusError = true;
                mStatusMessageKey = "err_webRequest";
                clearWebRequestSerial( mWebRequest );
                mWebRequest = -1;
                mMenuButton.setVisible( true );
                blockQuitting( false );
                break;
            case 1: {
                char *result = getWebResultSerial( mWebRequest );
                clearWebRequestSerial( mWebRequest );
                mWebRequest = -1;
                     
                printf( "Web result = %s\n", result );
   
                if( strstr( result, "DENIED" ) != NULL ) {
                    mStatusError = true;
                    mStatusMessageKey = "houseBeingRobbedOrEdited";
                    mMenuButton.setVisible( true );
                    blockQuitting( false );
                    }
                else if( strstr( result, "RECLAIMED" ) != NULL ) {
                    mStatusError = true;
                    mStatusMessageKey = "houseReclaimed";
                    mMenuButton.setVisible( true );
                    blockQuitting( false );
                    }
                else if( strstr( result, "CHILLING" ) != NULL ) {
                    mStatusError = true;
                    mStatusMessageKey = "houseChilling";
                    mMenuButton.setVisible( true );
                    blockQuitting( false );
                    }
                else if( strstr( result, "NEED_BACKPACK" ) != NULL ) {
                    mStatusError = true;
                    mStatusMessageKey = "needBackpack";
                    mMenuButton.setVisible( true );
                    blockQuitting( false );
                    }
                else if( strstr( result, "SYNCH" ) != NULL ) {
                    // house checked out; to be robbed synchronously.
                    
                    SimpleVector<char *> *tokens =
                        tokenizeString( result );
                    
                    if( tokens->size() != 16 ||
                        strcmp( *( tokens->getElement( 15 ) ), "OK" ) != 0 ) {
                        mStatusError = true;
                        mStatusMessageKey = "err_badServerResponse";
                        mMenuButton.setVisible( true );
                    
                        for( int i=0; i<tokens->size(); i++ ) {
                            delete [] *( tokens->getElement( i ) );
                            }
                        }
                    else {
                        mOwnerName = nameParse( *( tokens->getElement( 1 ) ) );

                        mHouseMap = *( tokens->getElement( 2 ) );

                        mBackpackContents = *( tokens->getElement( 3 ) );
                        mGalleryContents = *( tokens->getElement( 4 ) );

                        mWifeMoney = 0;
                        sscanf( *( tokens->getElement( 5 ) ),
                                "%d", &mWifeMoney );
                        mMusicSeed = 0;
                        sscanf( *( tokens->getElement( 6 ) ),
                                "%d", &mMusicSeed );
                        
                        mWifeName = *( tokens->getElement( 7 ) );
                        mSonName = *( tokens->getElement( 8 ) );
                        mDaughterName = *( tokens->getElement( 9 ) );
                        
                        mMaxSeconds = 0;
                        sscanf( *( tokens->getElement( 10 ) ),
                                "%d", &mMaxSeconds );

                        char *hostname = *( tokens->getElement( 11 ) );
                        int port = 0;
                        sscanf( *( tokens->getElement( 12 ) ),
                                "%d", &port );
                        int gameId = 0;
                        sscanf( *( tokens->getElement( 13 ) ),
                                "%d", &gameId );
                        char *gameCred = *( tokens->getElement( 14 ) );

                        printf( "OwnerName = %s\n", mOwnerName );
                        printf( "HouseMap = %s\n", mHouseMap );
                        printf( "Backpack = %s\n", mBackpackContents );
                        printf( "Gallery = %s\n", mGalleryContents );
                        printf( "WifeMoney = %d\n", mWifeMoney );
                        printf( "MusicSeed = %d\n", mMusicSeed );
                        printf( "Wife = %s\n", mWifeName );
                        printf( "Son = %s\n", mSonName );
                        printf( "Daughter = %s\n", mDaughterName );
                        printf( "MaxSeconds = %d\n", mMaxSeconds );
                        printf( "Hostname = %s\n", hostname );
                        printf( "Port = %d\n", port );
                        printf( "GameId = %d\n", gameId );
                        printf( "GameCred = %s\n", gameCred );

                        mSynchSocket = connectSynch(
                                hostname, port, gameId, gameCred );
                        if ( mSynchSocket ) {
                            // reset ping time, because house check-out
                            // counts as a ping
                            LiveHousePage::sLastPingTime = game_time( NULL );
                            }
                        else {
                            printf( "Failed to set up synchronous robbery!\n" );
                            }

                        // don't delete hostname - it's handled by HostAddress

                        delete [] *( tokens->getElement( 5 ) );
                        delete [] *( tokens->getElement( 6 ) );
                        delete [] *( tokens->getElement( 10 ) );
                        delete [] *( tokens->getElement( 12 ) );
                        delete [] *( tokens->getElement( 13 ) );
                        delete [] *( tokens->getElement( 14 ) );
                        }
                    
                    delete tokens;
                    }
                else {
                    // house checked out!
                    
                    SimpleVector<char *> *tokens =
                        tokenizeString( result );
                    
                    if( tokens->size() != 11 ||
                        strcmp( *( tokens->getElement( 10 ) ), "OK" ) != 0 ) {
                        mStatusError = true;
                        mStatusMessageKey = "err_badServerResponse";
                        mMenuButton.setVisible( true );
                    
                        for( int i=0; i<tokens->size(); i++ ) {
                            delete [] *( tokens->getElement( i ) );
                            }
                        }
                    else {
                        mOwnerName = nameParse( *( tokens->getElement( 0 ) ) );

                        mHouseMap = 
                            sha1Decrypt( mMapEncryptionKey,
                                         *( tokens->getElement( 1 ) ) );
                        delete [] *( tokens->getElement( 1 ) );
                        

                        mBackpackContents = *( tokens->getElement( 2 ) );
                        mGalleryContents = *( tokens->getElement( 3 ) );

                        mWifeMoney = 0;
                        sscanf( *( tokens->getElement( 4 ) ),
                                "%d", &mWifeMoney );
                        mMusicSeed = 0;
                        sscanf( *( tokens->getElement( 5 ) ),
                                "%d", &mMusicSeed );
                        
                        mWifeName = *( tokens->getElement( 6 ) );
                        mSonName = *( tokens->getElement( 7 ) );
                        mDaughterName = *( tokens->getElement( 8 ) );
                        
                        mMaxSeconds = 0;
                        sscanf( *( tokens->getElement( 9 ) ),
                                "%d", &mMaxSeconds );

                        printf( "OwnerName = %s\n", mOwnerName );
                        // printf( "HouseMap = %s\n", mHouseMap );
                        printf( "Backpack = %s\n", mBackpackContents );
                        printf( "Gallery = %s\n", mGalleryContents );
                        printf( "WifeMoney = %d\n", mWifeMoney );
                        printf( "MusicSeed = %d\n", mMusicSeed );
                        printf( "Wife = %s\n", mWifeName );
                        printf( "Son = %s\n", mSonName );
                        printf( "Daughter = %s\n", mDaughterName );
                        printf( "MaxSeconds = %d\n", mMaxSeconds );


                        delete [] *( tokens->getElement( 4 ) );
                        delete [] *( tokens->getElement( 5 ) );
                        delete [] *( tokens->getElement( 9 ) );
                        delete [] *( tokens->getElement( 10 ) );

                        // reset ping time, because house check-out
                        // counts as a ping
                        LiveHousePage::sLastPingTime = game_time( NULL );
                        }
                    
                    delete tokens;
                    }
                        
                        
                delete [] result;
                }
                break;
            }
        }
    }


Socket *RobCheckoutHousePage::connectSynch( char *hostname, int port,
        int gameId, const char *gameCred ) {
    SocketClient client;
    HostAddress addr( hostname, port );
    Socket *sock = client.connectToServer( &addr );
    if ( sock == NULL )
        return NULL;

    char *sendBuf = autoSprintf( "start_rob\n%d\n%s\n[END_REQUEST]", gameId, gameCred );
    sock->send( (unsigned char*) sendBuf, strlen(sendBuf) );
    delete [] sendBuf;

    char *line = readLine( sock );
    if ( line && strcmp( line,"SYNCH" ) == 0 ) {
        delete [] line;
        return sock;
        }
    else {
        if ( line ) {
            delete [] line;
            }
        delete sock;
        return NULL;
        }

    }

        
void RobCheckoutHousePage::makeActive( char inFresh ) {
    if( !inFresh ) {
        return;
        }

    clearDataMembers();
        
    char *ticketHash = getTicketHash();




    if( mMapEncryptionKey != NULL ) {
        delete [] mMapEncryptionKey;
        }

    mMapEncryptionKey = getMapEncryptionKey();

    

    char *fullRequestURL = autoSprintf( 
        "%s?action=start_rob_house&user_id=%d&to_rob_home_id=%d"
        "&to_rob_character_name=%s&map_encryption_key=%s&%s",
        serverURL, userID, mToRobHomeID, mToRobCharacterName, 
        mMapEncryptionKey, ticketHash );
    delete [] ticketHash;
    
    mWebRequest = startWebRequestSerial( "GET", 
                                   fullRequestURL, 
                                   NULL );

    delete [] fullRequestURL;
    
    mMenuButton.setVisible( false );

    mStatusError = false;
    mStatusMessageKey = NULL;

    mReturnToMenu = false;
    
    setWaiting( true );
    }

