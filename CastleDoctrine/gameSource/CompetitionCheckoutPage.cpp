#include "CompetitionCheckoutPage.h"
#include "ticketHash.h"
#include "message.h"
#include "LiveHousePage.h"

#include "serialWebRequests.h"

#include "readLine.h"

#include "minorGems/game/Font.h"
#include "minorGems/game/game.h"

#include "minorGems/util/stringUtils.h"


extern Font *mainFont;


extern char *serverURL;

extern int userID;



CompetitionCheckoutPage::CompetitionCheckoutPage() 
        : mWebRequest( -1 ),
          mHouseMap( NULL ),
          mStepCost( 0 ),
          mBestScore( 0 ),
          mPriceList( NULL ),
          mMenuButton( mainFont, 4, -4, translate( "returnMenu" ) ),
          mReturnToMenu( false ) {

    addComponent( &mMenuButton );
    mMenuButton.addActionListener( this );
    }


void CompetitionCheckoutPage::clearDataMembers() {
    if( mHouseMap != NULL ) {
        delete [] mHouseMap;
        }
    mHouseMap = NULL;
    if( mPriceList != NULL ) {
        delete [] mPriceList;
        }
    mPriceList = NULL;
    }


        
CompetitionCheckoutPage::~CompetitionCheckoutPage() {
    if( mWebRequest != -1 ) {
        clearWebRequestSerial( mWebRequest );
        }
    
    clearDataMembers();
    }



void CompetitionCheckoutPage::setCompetitionID( int inCompetitionID ) {
    mCompetitionID = inCompetitionID;
    }



char CompetitionCheckoutPage::getReturnToMenu() {
    return mReturnToMenu;
    }



char *CompetitionCheckoutPage::getHouseMap() {
    if( mHouseMap == NULL ) {
        return NULL;
        }
    else {
        char *returnValue = stringDuplicate( mHouseMap );
        
        return returnValue;
        }
    }

char *CompetitionCheckoutPage::getPriceList() {
    if( mPriceList == NULL ) {
        return NULL;
        }
    else {
        return stringDuplicate( mPriceList );
        }
    }

int CompetitionCheckoutPage::getStepCost() {
    return mStepCost;
    }

int CompetitionCheckoutPage::getBestScore() {
    return mBestScore;
    }

int CompetitionCheckoutPage::getCompetitionID() {
    return mCompetitionID;
    }

int CompetitionCheckoutPage::getTimeout() {
    return mTimeout;
    }

int CompetitionCheckoutPage::getMusicSeed() {
    return mMusicSeed;
    }


void CompetitionCheckoutPage::actionPerformed( GUIComponent *inTarget ) {
    if( inTarget == &mMenuButton ) {
        mReturnToMenu = true;
        }
    }


void CompetitionCheckoutPage::step() {
    if( mWebRequest != -1 ) {
            
        int stepResult = stepWebRequestSerial( mWebRequest );
        
        if( stepResult != 0 ) {
            setWaiting( false );
            }

        switch( stepResult ) {
            case 0:
                break;
            case -1:
                mStatusError = true;
                mStatusMessageKey = "err_webRequest";
                clearWebRequestSerial( mWebRequest );
                mWebRequest = -1;
                mMenuButton.setVisible( true );
                break;
            case 1: {
                char *result = getWebResultSerial( mWebRequest );
                clearWebRequestSerial( mWebRequest );
                mWebRequest = -1;
                     
                printf( "Web result = %s\n", result );
   
                if( strstr( result, "DENIED" ) != NULL ) {
                    mStatusError = true;
                    mStatusMessageKey = "competitionDenied";
                    mMenuButton.setVisible( true );
                    }
                else {
                    // house checked out!
                    
                    SimpleVector<char *> *tokens =
                        tokenizeString( result );
                    
                    if( tokens->size() != 7 ||
                        strcmp( *( tokens->getElement( 6 ) ), "OK" ) != 0 ) {
                        mStatusError = true;
                        mStatusMessageKey = "err_badServerResponse";
                        mMenuButton.setVisible( true );
                    
                        for( int i=0; i<tokens->size(); i++ ) {
                            delete [] *( tokens->getElement( i ) );
                            }
                        }
                    else {
                        mHouseMap = *( tokens->getElement( 0 ) );
                        
                        mBestScore = 0;
                        sscanf( *( tokens->getElement( 1 ) ),
                                "%d", &mBestScore );

                        mStepCost = 0;
                        sscanf( *( tokens->getElement( 2 ) ),
                                "%d", &mStepCost );

                        mPriceList = *( tokens->getElement( 3 ) );

                        mTimeout = 0;
                        sscanf( *( tokens->getElement( 4 ) ),
                                "%d", &mTimeout );

                        mMusicSeed = 0;
                        sscanf( *( tokens->getElement( 5 ) ),
                                "%d", &mMusicSeed );
                        
                        delete [] *( tokens->getElement( 1 ) );
                        delete [] *( tokens->getElement( 2 ) );
                        delete [] *( tokens->getElement( 4 ) );
                        delete [] *( tokens->getElement( 5 ) );

                        printf( "HouseMap = %s\n", mHouseMap );
                        printf( "BestScore = %d\n", mBestScore );
                        printf( "StepCost = %d\n", mStepCost );
                        printf( "PriceList = %s\n", mPriceList );
                        printf( "Timeout = %d\n", mTimeout );
                        printf( "MusicSeed = %d\n", mMusicSeed );

                        // reset ping time, because house check-out
                        // counts as a ping
                        LiveHousePage::sLastPingTime = game_time( NULL );
                        }
                    
                    delete tokens;
                    }
                        
                        
                delete [] result;
                }
                break;
            }
        }
    }


void CompetitionCheckoutPage::makeActive( char inFresh ) {
    if( !inFresh ) {
        return;
        }

    clearDataMembers();
        
    char *ticketHash = getTicketHash();

    char *fullRequestURL = autoSprintf( 
        "%s?action=start_competition&user_id=%d&competition_id=%d&%s",
        serverURL, userID, mCompetitionID, ticketHash );
    delete [] ticketHash;
    
    mWebRequest = startWebRequestSerial( "GET", 
                                   fullRequestURL, 
                                   NULL );

    delete [] fullRequestURL;
    
    mMenuButton.setVisible( false );

    mStatusError = false;
    mStatusMessageKey = NULL;

    mReturnToMenu = false;
    
    setWaiting( true );
    }
