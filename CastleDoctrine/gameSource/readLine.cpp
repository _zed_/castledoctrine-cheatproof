#include <stdio.h>

#include "minorGems/util/stringUtils.h"
#include "minorGems/network/Socket.h"

#define READ_SIZE  50

static char readBuffer[ READ_SIZE + 1 ];

char *readLine( Socket *inSock, bool nonblocking ) {
    
    char *readSoFar = stringDuplicate( "" );

    char error = false;

    int timeout = 1000;
    if ( nonblocking )
        timeout = 1;
    
    while( !(*readSoFar && readSoFar[strlen(readSoFar)-1] == '\n') && ! error ) {
        
        int numRead = inSock->receive( (unsigned char *)readBuffer, 
                                       READ_SIZE, timeout );
    
        if( numRead >= 0 ) {
            readBuffer[ numRead ] = '\0';
            
            char *newReadSoFar = concatonate( readSoFar, readBuffer );
            
            delete [] readSoFar;
            
            readSoFar = newReadSoFar;
            }
        else if( numRead != -2 ) {
            // not timeout, real error
            error = true;
            }
        else if( nonblocking && strlen(readSoFar) == 0 )
            return NULL;
        }
    
    if( error ) {
        delete [] readSoFar;
        return NULL;
        }
    
    char *trimmedMessage = trimWhitespace( readSoFar );
    
    delete [] readSoFar;

    return trimmedMessage;
    }
