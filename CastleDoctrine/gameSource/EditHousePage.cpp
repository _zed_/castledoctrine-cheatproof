#include "EditHousePage.h"

#include "message.h"
#include "balance.h"


#include "minorGems/game/Font.h"
#include "minorGems/game/game.h"
#include "minorGems/game/drawUtils.h"

#include "minorGems/util/stringUtils.h"
#include "minorGems/util/SettingsManager.h"

#include "minorGems/io/file/File.h"
#include "minorGems/io/file/Path.h"






extern Font *mainFont;
extern Font *mainFontFixed;


extern int diffHighlightsOff;



EditHousePage::EditHousePage() 
        : mStartHouseMap( NULL ),
          mVaultContents( NULL ),
          mBackpackContents( NULL ),
          mPriceList( NULL ),
          // starts empty
          mPurchaseList( stringDuplicate( "#" ) ),
          mSellList( stringDuplicate( "#" ) ),
          mObjectPicker( 8, 5 ),
          mGridDisplay( 0, 0, &mObjectPicker ),
          mDoneButton( mainFont, 8, -5, translate( "doneEdit" ) ),
          mBackpackButton( mainFont, 8, -3.5, translate( "loadBackpack" ) ),
          mAuctionButton( mainFont, -8, -4, translate( "openAuctionList" ) ),
          mLoadButton( mainFont, -9, -5.2, translate( "loadHouse" ) ),
          mSaveButton( mainFont, -7, -5.2, translate( "saveHouse" ) ),
          mFilenameBox( mainFontFixed, mainFont, -7.65, -6.1, 4, false, translate( "fileBoxLabel" ), NULL, NULL, true ),
          mUndoButton( mainFont, 8, -0.5, translate( "undo" ), 'z', 'Z' ),
          mJumpToTapesButton( mainFont, 8, -2.25, translate( "jumpToTapes" ) ),
          mSuicideButton( mainFont, 8, -1, translate( "suicide" ) ),
          mSuicideConfirmCheckbox( 8, -.125, 1/16.0 ),
          mDiffHighlightToggleButton( "diffHighlightsOn.tga", 
                                      "diffHighlightsOff.tga", 
                                      8, -1.75, 1/16.0 ),
          mEyedropperStatus( 6.5, 5, 1/16.0 ),
          mBlockSuicideButton( false ),
          mGallery( mainFont, -8, 1 ),
          mNumberOfTapes( 0 ),
          mJumpToTapes( false ),
          mDone( false ),
          mDead( false ) {

    addComponent( &mDoneButton );
    addComponent( &mJumpToTapesButton );
    addComponent( &mSuicideButton );
    addComponent( &mSuicideConfirmCheckbox );
    addComponent( &mBackpackButton );
    addComponent( &mAuctionButton );
    addComponent( &mFilenameBox );
    addComponent( &mLoadButton );
    addComponent( &mSaveButton );
    addComponent( &mUndoButton );
    addComponent( &mGridDisplay );
    
    addComponent( &mDiffHighlightToggleButton );
    
    addComponent( &mEyedropperStatus );
    
    addComponent( &mObjectPicker );


    mDoneButton.setMouseOverTip( "" );
    mLoadButton.setMouseOverTip( translate( "loadHouseTip" ) );
    mSaveButton.setMouseOverTip( translate( "saveHouseTip" ) );
    mUndoButton.setMouseOverTip( translate( "undoTip" ) );
    mBackpackButton.setMouseOverTip( translate( "loadBackpackTip" ) );
    mAuctionButton.setMouseOverTip( translate( "openAuctionListTip" ) );

    mJumpToTapesButton.setMouseOverTip( translate( "jumpToTapesTip" ) );
    
    mSuicideButton.setMouseOverTip( translate( "unconfirmedSuicideTip" ) );
    mSuicideConfirmCheckbox.setMouseOverTip( 
        translate( "suicideConfirmTip" ) );
    mSuicideConfirmCheckbox.setMouseOverTipB( 
        translate( "suicideConfirmTip" ) );

    mDoneButton.addActionListener( this );
    
    mJumpToTapesButton.addActionListener( this );
    mSuicideButton.addActionListener( this );
    mSuicideConfirmCheckbox.addActionListener( this );
    mBackpackButton.addActionListener( this );
    mAuctionButton.addActionListener( this );
    mFilenameBox.addActionListener( this );
    mLoadButton.addActionListener( this );
    mLoadButton.setVisible( false );
    mSaveButton.addActionListener( this );
    mSaveButton.setVisible( false );
    mUndoButton.addActionListener( this );
    mUndoButton.setVisible( false );
    mGridDisplay.addActionListener( this );
    mObjectPicker.addActionListener( this );


    mDiffHighlightToggleButton.addActionListener( this );

    mDiffHighlightToggleButton.setMouseOverTip( 
        translate( "diffHighlightsOff" ) );
    mDiffHighlightToggleButton.setMouseOverTipB( 
        translate( "diffHighlightsOn" ) );
    
    
    
    addComponent( &mGallery );
    }


        
EditHousePage::~EditHousePage() {
    
    if( mStartHouseMap != NULL ) {
        delete [] mStartHouseMap;
        }

    if( mVaultContents != NULL ) {
        delete [] mVaultContents;
        }

    if( mBackpackContents != NULL ) {
        delete [] mBackpackContents;
        }

    if( mPriceList != NULL ) {
        delete [] mPriceList;
        }

    if( mPurchaseList != NULL ) {
        delete [] mPurchaseList;
        }
    if( mSellList != NULL ) {
        delete [] mSellList;
        }
    }



void EditHousePage::setWifeName( const char *inWifeName ) {
    mGridDisplay.setWifeName( inWifeName );
    mObjectPicker.setWifeName( inWifeName );
    }

void EditHousePage::setSonName( const char *inSonName ) {
    mGridDisplay.setSonName( inSonName );
    }

void EditHousePage::setDaughterName( const char *inDaughterName ) {
    mGridDisplay.setDaughterName( inDaughterName );
    }


char *EditHousePage::getWifeName() {
    return mGridDisplay.getWifeName();
    }

char *EditHousePage::getSonName() {
    return mGridDisplay.getSonName();
    }

char *EditHousePage::getDaughterName() {
    return mGridDisplay.getDaughterName();
    }



void EditHousePage::setHouseMap( const char *inHouseMap ) {
    if( mStartHouseMap != NULL ) {
        delete [] mStartHouseMap;
        }
    mStartHouseMap = stringDuplicate( inHouseMap );
    
    mGridDisplay.setHouseMap( inHouseMap );

    mUndoButton.setVisible( mGridDisplay.canUndo() );

    mDoneButton.setVisible( 
        mGridDisplay.areMandatoriesPlaced()
        &&
        mGridDisplay.doAllFamilyObjectsHaveExitPath() );
    
    mChangesCost = 0;
    mDiffHighlightToggleButton.setVisible( false );

    mMapStartedOutEmpty = mGridDisplay.getMapStartedOutEmpty();

    mBackpackOrVaultChanged = false;
    }



char *EditHousePage::getHouseMap() {
    return mGridDisplay.getHouseMap();
    }



void EditHousePage::setVaultContents( const char *inVaultContents ) {
    if( mVaultContents != NULL ) {
        delete [] mVaultContents;
        }
    mVaultContents = stringDuplicate( inVaultContents );
    }



char *EditHousePage::getVaultContents() {
    return stringDuplicate( mVaultContents );
    }



void EditHousePage::setBackpackContents( const char *inBackpackContents ) {
    if( mBackpackContents != NULL ) {
        delete [] mBackpackContents;
        }
    mBackpackContents = stringDuplicate( inBackpackContents );
    }



char *EditHousePage::getBackpackContents() {
    return stringDuplicate( mBackpackContents );
    }



void EditHousePage::setGalleryContents( const char *inGalleryContents ) {
    
    mGallery.setGalleryContents( inGalleryContents );
    }


void EditHousePage::setNumberOfTapes( int inNumber ) {
    mNumberOfTapes = inNumber;
    }


char *EditHousePage::getGalleryContents() {
    return mGallery.getGalleryContents();
    }





char *EditHousePage::getFamilyExitPaths() {
    return mGridDisplay.getFamilyExitPaths();
    }


char EditHousePage::getWifeLiving() {
    return mGridDisplay.getWifeLiving();
    }


char *EditHousePage::getPurchaseList() {
    return stringDuplicate( mPurchaseList );
    }


void EditHousePage::setPurchaseList( const char *inPurchaseList ) {
    if( mPurchaseList != NULL ) {
        delete [] mPurchaseList;
        }
    mPurchaseList = stringDuplicate( inPurchaseList );
    }




char *EditHousePage::getSellList() {
    return stringDuplicate( mSellList );
    }


void EditHousePage::setSellList( const char *inSellList ) {
    if( mSellList != NULL ) {
        delete [] mSellList;
        }
    mSellList = stringDuplicate( inSellList );
    }




void EditHousePage::setPriceList( const char *inPriceList ) {
    if( mPriceList != NULL ) {
        delete [] mPriceList;
        }
    mPriceList = stringDuplicate( inPriceList );

    mObjectPicker.setPriceList( inPriceList );
    }



char *EditHousePage::getPriceList() {
    return stringDuplicate( mPriceList );
    }



void EditHousePage::setLootValue( int inLootValue ) {
    mLootValue = inLootValue;
    mBlockSuicideButton = false;

    checkIfPlacementAllowed();
    }

void EditHousePage::setReserve( int inReserve ) {
    mReserve = inReserve;
    }

void EditHousePage::setLootFake( bool inLootFake ) {
    mLootFake = inLootFake;
    mBackpackButton.setVisible( ! inLootFake );
    }

void EditHousePage::setMustSelfTest( char inMustSelfTest ) {
    mMustSelfTest = inMustSelfTest;
    
    // if house damaged and needs a self test, don't allow auctions
    // (server blocks auction activities anyway in this case)
    mAuctionButton.setVisible( !mMustSelfTest );
    }



void EditHousePage::checkIfPlacementAllowed() {
    // always allow placement with new accounting method
    mGridDisplay.allowPlacement( true );
        
    // can't afford to place anything, house not edited yet
    // allow suicide
    mSuicideButton.setVisible(
        ! mBlockSuicideButton &&
        ! mUndoButton.isVisible() );

    if( mSuicideButton.isVisible() ) {
        mSuicideConfirmCheckbox.setVisible( true );
        mSuicideConfirmCheckbox.setToggled( false );
        mSuicideButton.setMouseOverTip( translate( "unconfirmedSuicideTip" ) );
        }
    else {
        mSuicideConfirmCheckbox.setVisible( false );
        }

    checkIfTapesButtonVisible();
    }



void EditHousePage::checkIfDoneButtonVisible() {
    // can't click DONE if house has no goal set
    // or family blocked
    // or spent more than we have on changes to house
    mDoneButton.setVisible( 
        mGridDisplay.areMandatoriesPlaced()
        &&
        mGridDisplay.doAllFamilyObjectsHaveExitPath()
        && (
            mLootValue - mChangesCost >= 0
            || ( mChangesCost == 0
                && strcmp( mVaultContents, "#" ) == 0
                && strcmp( mBackpackContents, "#" ) == 0 ) ) );
    }



char EditHousePage::houseMapChanged() {

    if( mGridDisplay.getEdited() ) {
        // some edits to report, whether or not map was actually changed
        // by edits, count it as changed
        return true;
        }
    

    if( mStartHouseMap == NULL || mMustSelfTest ) {
        return true;
        }

    char *newMap = mGridDisplay.getHouseMap();

    int comp = strcmp( newMap, mStartHouseMap );

    if( comp != 0 ) {
        
        printf( "House maps differ.  Old:\n%s\n\nNew:\n%s\n\n", mStartHouseMap,
                newMap );
        }
    
    
    delete [] newMap;
    
    if( comp == 0 ) {
        return false;
        }


    return true;
    }




void EditHousePage::recomputeChangeCost() {
    mChangesCost = 0;
        
    SimpleVector<GridDiffRecord> diffList = mGridDisplay.getEditDiff();
    
    int numRecords = diffList.size();
    
    for( int i=0; i<numRecords; i++ ) {
        GridDiffRecord *r = diffList.getElement( i );
        
        mChangesCost += 
            r->placementCount *
            mObjectPicker.getPrice( r->objectID );
        }
        
    mGridDisplay.setTouchedHighlightRed( mChangesCost > mLootValue );

    mDiffHighlightToggleButton.setVisible( !mMapStartedOutEmpty && 
                                           mChangesCost > 0 );
    }


    
File *EditHousePage::openHouseFile( const char *inFilename ) {
    char **pathSteps = new char*[1];

    pathSteps[0] = (char *) "savedHouses";

    File *dirFile = new File( NULL, pathSteps[0] );
    dirFile->makeDirectory();
    delete dirFile;

    File *houseFile = new File( new Path( pathSteps, 1, false ), inFilename );

    delete [] pathSteps;

    return houseFile;
    }

void EditHousePage::checkIfFileButtonsVisible() {
    const char *filename = mFilenameBox.getText();
    const bool fnNull = strlen( filename ) == 0;

    mSaveButton.setVisible( ! fnNull && mGridDisplay.areMandatoriesPlaced() );

    File *houseFile = openHouseFile( filename );
    mLoadButton.setVisible( !fnNull && houseFile->exists() );
    delete houseFile;
    }

void EditHousePage::actionPerformed( GUIComponent *inTarget ) {
    if( inTarget == &mSuicideConfirmCheckbox ) {
        if( mSuicideConfirmCheckbox.getToggled() ) {
            mSuicideButton.setMouseOverTip( 
                translate( "suicideTip" ) );
            }
        else {
            mSuicideButton.setMouseOverTip( 
                translate( "unconfirmedSuicideTip" ) );
            }    
        }
    else if( inTarget == &mGridDisplay ) {
        int cost = 
            mObjectPicker.getPrice( mGridDisplay.getLastPlacedObject() );

        mUndoButton.setVisible( mGridDisplay.canUndo() );
        
        if( cost != -1 && ! mGridDisplay.wasLastActionPlayerMotion() ) {
            mObjectPicker.useSelectedObject();

            checkIfPlacementAllowed();
            }

        if( mGridDisplay.didLastActionChangeDiff() ) {    
            recomputeChangeCost();
            }
        
        checkIfDoneButtonVisible();
        checkIfFileButtonsVisible();

        // change to house map
        actionHappened();
        }
    else if( inTarget == &mDiffHighlightToggleButton ) {
        diffHighlightsOff = mDiffHighlightToggleButton.getToggled();
        
        SettingsManager::setSetting( "diffHighlightsOff", diffHighlightsOff );
        mGridDisplay.toggleTouchedHighlights( ! diffHighlightsOff );
        }
    else if( inTarget == &mBackpackButton ) {
        mShowLoadBackpack = true;
        }
    else if( inTarget == &mAuctionButton ) {
        mShowAuctions = true;
        }
    else if( inTarget == &mJumpToTapesButton ) {
        mJumpToTapes = true;
        }
    else if( inTarget == &mDoneButton ) {
        
        // Reset any states
        // that were toggled by the last robber.

        // We show the house-as-robbed view to the owner once,
        // until they perform their first complete edit, and THEN toggle
        // everything back.

        mGridDisplay.resetToggledStates( 0 );


        mDone = true;
        }
    else if( inTarget == &mSuicideButton ) {
        if( mSuicideConfirmCheckbox.isVisible() && 
            ! mSuicideConfirmCheckbox.getToggled() ) {
            return;
            }
        mGridDisplay.resetToggledStates( 0 );

        mDead = true;
        mDone = true;
        }
    else if( inTarget == &mObjectPicker ) {
        if( mObjectPicker.shouldShowGridView() ) {
            mShowGridObjectPicker = true;
            }
        else {
            // change in picked object
            checkIfPlacementAllowed();
            }
        }
    else if( inTarget == &mUndoButton ) {
        
        mBlockSuicideButton = true;

        mGridDisplay.undo();
        
        
        mUndoButton.setVisible( mGridDisplay.canUndo() );

        checkIfPlacementAllowed();
                
        recomputeChangeCost();
        checkIfDoneButtonVisible();
        checkIfFileButtonsVisible();

        // change to house map
        actionHappened();
        }    
    else if( inTarget == &mFilenameBox ) {
        checkIfFileButtonsVisible();
        }
    else if( inTarget == &mLoadButton ) {
        File *houseFile = openHouseFile( mFilenameBox.getText() );
        const char* house = houseFile->readFileContents();
        delete houseFile;

        if( !house ) {
            return;
            }

        mGridDisplay.loadHouseMap( house );
        delete [] house;

        // TODO: reset tiles to base state

        recomputeChangeCost();
        checkIfDoneButtonVisible();
        checkIfFileButtonsVisible();
        mUndoButton.setVisible( mGridDisplay.canUndo() );
        checkIfPlacementAllowed();
        actionHappened();
        }    
    else if( inTarget == &mSaveButton ) {
        File *houseFile = openHouseFile( mFilenameBox.getText() );
        const char *houseMap = getHouseMap();
        houseFile->writeToFile( houseMap );
        delete [] houseMap;
        delete houseFile;

        checkIfFileButtonsVisible();
        }    
    }





        
void EditHousePage::makeActive( char inFresh ) {
    LiveHousePage::makeActive( inFresh );
    
    if( !inFresh ) {
        return;
        }
    
    mGridDisplay.clearMovementKeyHolds();
    

    mJumpToTapes = false;
    mDone = false;
    mDead = false;
    mShowLoadBackpack = false;
    mShowAuctions = false;
    mShowGridObjectPicker = false;

    checkIfDoneButtonVisible();
    checkIfFileButtonsVisible();
    checkIfTapesButtonVisible();

    blockQuitting( areRequestsPending() );

    mDiffHighlightToggleButton.setToggled( diffHighlightsOff );
    mGridDisplay.toggleTouchedHighlights( ! mMapStartedOutEmpty && 
                                          ! diffHighlightsOff );
    }



void EditHousePage::step() {
    LiveHousePage::step();
    
    
    checkIfTapesButtonVisible();
    
    blockQuitting( areRequestsPending() );
    }




void EditHousePage::checkIfTapesButtonVisible() {
    // can jump to tapes as long as no editing done yet and no purchase/sale
    // done yet (so nothing will be lost when we abandon the house edit)
    // AND no background requests to server are pending
    mJumpToTapesButton.setVisible( ! areRequestsPending() && 
                                   mNumberOfTapes > 0 &&
                                   ! mUndoButton.isVisible() &&
                                   ! mBackpackOrVaultChanged );
    }




extern Font *numbersFontFixed;


void EditHousePage::drawUnderComponents( doublePair inViewCenter, 
                                         double inViewSize ) {
    int totalCost = mChangesCost;
    const char *balanceHeader = "editBalance";
    if( mLootFake ) {
        balanceHeader = "fakeEditBalance";
        }
    drawBalance( mLootValue, totalCost, mLootFake, balanceHeader );
    }



void EditHousePage::draw( doublePair inViewCenter, 
                               double inViewSize ) {
        
    doublePair labelPos = { 0, 7 };
    
    drawMessage( "editDescription", labelPos, false );    


    if( ! mGridDisplay.doAllFamilyObjectsHaveExitPath() ) {
        
        // explanation for why Done button hidden

        doublePair buttonPos = mDoneButton.getPosition();
        
        buttonPos.y += 0.5;

        drawMessage( "familyExitMessage", buttonPos, true );
        }
    
    }




void EditHousePage::keyDown( unsigned char inASCII ) {
    if( inASCII == '+' ) {
        mGridDisplay.saveWholeMapImage();
        }
    
    }
