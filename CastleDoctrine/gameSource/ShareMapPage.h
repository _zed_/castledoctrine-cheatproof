#include "GamePage.h"

#include "TextField.h"
#include "TextButton.h"

#include "minorGems/ui/event/ActionListener.h"


class ShareMapPage : public GamePage, public ActionListener
{
        
    public:
        
        ShareMapPage();
        
        virtual ~ShareMapPage();

        virtual char getDone();
    
        virtual void actionPerformed( GUIComponent *inTarget );

        virtual void step();
        
        virtual void draw( doublePair inViewCenter, 
                           double inViewSize );
        
        virtual void makeActive( char inFresh );
    
        virtual void setIsShareMapAfterDeath( char isShareMapAfterDeath, char isShareMapAfterDeathAllowed );
    
        virtual char getIsShareMapAfterDeath();
    
        virtual char getIsShareMapAfterDeathAllowed();
    
    protected:

        int mWebRequest;
    
        TextButton mDoneButton;
    
        char mDone;
    
        char mIsShareMapAfterDeath;
    
        char mIsShareMapAfterDeathAllowed;
    
        int mBotMapsAdded;
        int mBotMapsDeleted;
};

