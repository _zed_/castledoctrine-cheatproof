#include "GamePage.h"

#include "TextField.h"
#include "TextButton.h"
#include "RobPickList.h"


#include "minorGems/ui/event/ActionListener.h"




class CompetitionsMenuPage : public GamePage, public ActionListener {
        
    public:
        
        CompetitionsMenuPage();
        
        virtual ~CompetitionsMenuPage();


        virtual char getReturnToMenu();
        virtual char getStartCompetition();

        virtual int getCompetitionID();
        

        virtual void actionPerformed( GUIComponent *inTarget );


        virtual void step();
        
        virtual void draw( doublePair inViewCenter, 
                           double inViewSize );
        
        virtual void makeActive( char inFresh );

    protected:
        

        RobPickList mPickList;

        TextButton mMenuButton;
        TextButton mStartCompetitionButton;
        

        char mReturnToMenu;
        char mStartCompetition;
        
    };

