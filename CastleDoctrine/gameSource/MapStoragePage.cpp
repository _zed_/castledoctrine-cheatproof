#include "MapStoragePage.h"

#include "message.h"
#include "balance.h"


#include "minorGems/game/Font.h"
#include "minorGems/game/game.h"
#include "minorGems/game/drawUtils.h"

#include "minorGems/util/stringUtils.h"
#include "minorGems/util/SettingsManager.h"


extern Font *mainFont;


MapStoragePage::MapStoragePage()
        : mMenuButton( mainFont, 8, -5, translate( "returnMenu" ) ),
          mStoreRestoreConfirmCheckbox( 1.25, -5, 1/16.0 ),
          mReturnToMenu( false ),
          mStoreUserMap( false ),
          mRestoreUserMap( false ),
          mShowUserMap( false ),
          mShareMap( false ),
          mMapNumber( 0 ),
          mShareMapButton( mainFont, 0, -2, translate( "shareMap" ) )
{
    doublePair labelPos = { -6, 5 };
    
    for( int i = 0; i < NUM_USER_MAPS_MAX; ++i )
    {
        char* storeString = autoSprintf( "%s #%d", translate( "storeUserMap" ), i + 1 );
        char* restoreString = autoSprintf( "%s #%d", translate( "restoreUserMap" ), i + 1 );
        char* showString = autoSprintf( "%s #%d", translate( "showUserMap" ), i + 1 );
        
        mStoreUserMapButton[ i ] = new TextButton( mainFont, labelPos.x, labelPos.y - i * 2, storeString );
        mRestoreUserMapButton[ i ] = new TextButton( mainFont, labelPos.x + 6, labelPos.y - i * 2, restoreString );
        mShowUserMapButton[ i ] = new TextButton( mainFont, labelPos.x + 12, labelPos.y - i * 2, showString );
        
        addComponent( mStoreUserMapButton[ i ] );
        addComponent( mRestoreUserMapButton[ i ] );
        addComponent( mShowUserMapButton[ i ] );
        
        mStoreUserMapButton[ i ]->addActionListener( this );
        mRestoreUserMapButton[ i ]->addActionListener( this );
        mShowUserMapButton[ i ]->addActionListener( this );
        
        mStoreUserMapButton[ i ]->setMouseOverTip( translate( "storeUserMapTip" ) );
        mRestoreUserMapButton[ i ]->setMouseOverTip( translate( "restoreUserMapTip" ) );
        mShowUserMapButton[ i ]->setMouseOverTip( translate( "showUserMapTip" ) );
        
        delete [] storeString;
        delete [] restoreString;
        delete [] showString;
    }

    mStoreRestoreConfirmCheckbox.setMouseOverTip( translate( "storeRestoreConfirmTip" ) );
    mStoreRestoreConfirmCheckbox.setMouseOverTipB( translate( "storeRestoreConfirmTip" ) );
    mStoreRestoreConfirmCheckbox.setToggled( false );

    addComponent( &mStoreRestoreConfirmCheckbox );
    addComponent( &mShareMapButton );
    addComponent( &mMenuButton );
    
    mMenuButton.setMouseOverTip( "" );
    mShareMapButton.setMouseOverTip( translate( "shareMapTip" ) );
    
    mMenuButton.addActionListener( this );
    mShareMapButton.addActionListener( this );
    mStoreRestoreConfirmCheckbox.addActionListener( this );
}


        
MapStoragePage::~MapStoragePage()
{
    for( int i = 0; i < NUM_USER_MAPS_MAX; ++i )
    {
        delete mStoreUserMapButton[ i ];
        delete mRestoreUserMapButton[ i ];
        delete mShowUserMapButton[ i ];
    }
}


char MapStoragePage::getReturnToMenu()
{
    return mReturnToMenu;
}


char MapStoragePage::getStoreUserMap()
{
    return mStoreUserMap;
}


char MapStoragePage::getRestoreUserMap()
{
    return mRestoreUserMap;
}


char MapStoragePage::getShowUserMap()
{
    return mShowUserMap;
}


char MapStoragePage::getShareMap()
{
    return mShareMap;
}


int MapStoragePage::getMapNumber()
{
    return mMapNumber;
}


void MapStoragePage::actionPerformed( GUIComponent *inTarget )
{
    if( inTarget == &mMenuButton )
    {
        mReturnToMenu = true;
        return;
    }
    
    for( int i = 0; i < NUM_USER_MAPS_MAX; ++i )
    {
        if( inTarget == mStoreUserMapButton[ i ] )
        {
            // check if confirmation box was selected
            if( mStoreRestoreConfirmCheckbox.isVisible() &&
                ! mStoreRestoreConfirmCheckbox.getToggled() )
            {
                mStoreRestoreConfirmCheckbox.setIsHighlighting( true, true );
                return;
            }
        
            mMapNumber = i;
            mStoreUserMap = true;
            return;
        }
        else if( inTarget == mRestoreUserMapButton[ i ] )
        {
            if( mStoreRestoreConfirmCheckbox.isVisible() &&
                ! mStoreRestoreConfirmCheckbox.getToggled() )
            {
                mStoreRestoreConfirmCheckbox.setIsHighlighting( true, true );
                return;
            }
            mMapNumber = i;
            mRestoreUserMap = true;
            return;
        }
        else if( inTarget == mShowUserMapButton[ i ] )
        {
            mMapNumber = i;
            mShowUserMap = true;
            return;
        }
    } // end for
    
    if( inTarget == &mShareMapButton )
    {
        if( mStoreRestoreConfirmCheckbox.isVisible() &&
            ! mStoreRestoreConfirmCheckbox.getToggled() )
        {
            mStoreRestoreConfirmCheckbox.setIsHighlighting( true, true );
            return;
        }
    
        mShareMap = true;
        return;
    }
}

        
void MapStoragePage::makeActive( char inFresh )
{
    if( !inFresh )
    {
        return;
    }
    
    mStoreRestoreConfirmCheckbox.setToggled( false );

    mStoreUserMap = false;
    mRestoreUserMap = false;
    mShowUserMap = false;
    mReturnToMenu = false;
    mShareMap = false;
}


void MapStoragePage::draw( doublePair inViewCenter, double inViewSize )
{
    doublePair labelPos = { -4, -5 };
    
    drawMessage( translate( "mapStorageConfirmText" ), labelPos, false );
    
    labelPos.x = 0;
    labelPos.y = 7;
    
    drawMessage( translate( "mapStorageHeadline" ), labelPos, false );
}



