#include "GamePage.h"

#include "TextField.h"
#include "TextButton.h"

#include "minorGems/ui/event/ActionListener.h"


class BotMapVotePage : public GamePage, public ActionListener
{
        
    public:
        
        BotMapVotePage();
        
        virtual ~BotMapVotePage();

        virtual char getReturnToHome();
    
        virtual void step();
    
        virtual void makeActive( char inFresh );
    
        virtual void actionPerformed( GUIComponent *inTarget );
    
        virtual void setBotMapInfos( int botMapUserID, int botMapUserVote );
    
    protected:

        int mWebRequest;
    
        //TextButton mHomeButton;
    
        char mReturnToHome;
    
        int mBotMapUserVote;
    
        int mBotMapUserID;
};

