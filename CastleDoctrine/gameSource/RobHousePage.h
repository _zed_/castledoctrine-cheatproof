#include "LiveHousePage.h"

#include "TextField.h"
#include "TextButton.h"

#include "SpriteToggleButton.h"
#include "CheckboxButton.h"


#include "RobHouseGridDisplay.h"

#include "Gallery.h"

#include "inventory.h"



#include "minorGems/ui/event/ActionListener.h"
#include "minorGems/network/Socket.h"


#include <time.h>




class RobHousePage : public LiveHousePage, public ActionListener {
        
    public:
        
        RobHousePage( int inNumToolSlots );
        
        virtual ~RobHousePage();
        


        // destroyed by caller
        void setWifeName( const char *inWifeName );
        void setSonName( const char *inSonName );
        void setDaughterName( const char *inDaughterName );
        
        // destroyed by caller
        void setHouseMap( char *inHouseMap );
        char *getHouseMap();

        void jumpToMoveList( char *inMoveList );
        void undo( int undoMoves = 1 );

        void setSynchSocket( Socket *synchSock );
        Socket *getSynchSocket();

        // gets starting map (not map as modified by robber so far)
        // destroyed by caller
        char *getBlueprintMap();
        

        virtual int getVisibleOffsetX();
        virtual int getVisibleOffsetY();


        // destroyed by caller
        void setBackpackContents( char *inBackpackContents );
        char *getBackpackContents();

        void setGalleryContents( char *inGalleryContents );
        
        void setWifeMoney( int inMoney );

        void setMusicSeed( int inMusicSeed );
        
        void setMaxSeconds( int inMaxSeconds );

        void setStepCost( int inStepCost );

        void setInitCash( int inInitCash );

        void initialiseCompetition( int inCompetitionID, int inInitCash,
                int inStepCost, char *inPriceList );

        int getCompetitionID() {
            return mCompetitionID;
            }

        char getSuccess() {
            return mGridDisplay.getSuccess();
            }

        char *getMoveList() {
            return mGridDisplay.getMoveList();
            }


        char getWifeKilledRobber() {
            return mGridDisplay.getWifeKilledRobber();
            }
        

        char getWifeKilled() {
            return mGridDisplay.getWifeKilled();
            }

        char getWifeRobbed() {
            return mGridDisplay.getWifeRobbed();
            }

        char *getWifeName() {
            return mGridDisplay.getWifeName();
            }
        
        int getFamilyKilledCount() {
            return mGridDisplay.getFamilyKilledCount();
            }


        char getViewBlueprint() {
            return mViewBlueprint;
            }


        char getDone() {
            return mDone;
            }


        // destroyed by caller
        char *getPriceList();

        void setDescription( const char *inDescription );
        


        virtual void actionPerformed( GUIComponent *inTarget );


        virtual void step();
        
        virtual void draw( doublePair inViewCenter, 
                   double inViewSize );

        virtual void drawUnderComponents( doublePair inViewCenter, 
                                          double inViewSize );
        
        virtual void makeActive( char inFresh );

    protected:
        RobHouseGridDisplay mGridDisplay;
        TextButton mBlueprintButton;
        TextButton mUndoButton;
        TextButton mUndoManyButton;


        TextButton mDoneButton;
        CheckboxButton mSuicideConfirmCheckbox;

        SpriteToggleButton mMusicToggleButton;
        SpriteToggleButton mSafeMoveToggleButton;

        Gallery mGallery;
        
        HouseObjectPicker mToolPicker;
        
        int mMusicSeed;

        char mShowTimeAtEnd;
        time_t mEndTime;
        float mTimeMessageFade;
        float mTimeMessageFadeDirection;
        
        int mCompetitionID;
        
        int mStepCost;
        int mInitCash;
        int mToolCosts;

        InventorySlotButton *mToolSlots[ MAX_NUM_TOOL_SLOTS ];
        int mNumToolSlots;
        
        char mViewBlueprint;
        char mDone;

        char *mDescription;
        
        char *mBlueprints;

        char *mDeathMessage;

        char *mPriceList;

        
        // override from GamePage to selectively hide any waiting
        // icons (if robTime message shown, because it overlaps with waiting
        // icon)
        virtual char canShowWaitingIcon();
        
        void initToolSlots( int inNumToolSlots );

    };

