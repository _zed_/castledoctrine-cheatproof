#include "CompetitionCheckinPage.h"
#include "ticketHash.h"
#include "message.h"

#include "serialWebRequests.h"

#include "minorGems/game/Font.h"
#include "minorGems/game/game.h"

#include "minorGems/util/stringUtils.h"
#include "minorGems/network/web/URLUtils.h"


extern Font *mainFont;


extern char *serverURL;

extern int userID;


#define SLOT_Y 4.75


CompetitionCheckinPage::CompetitionCheckinPage() 
        : mWebRequest( -1 ),
          mPriceList( NULL ),
          mMoveList( NULL ),
          mSuccess( 0 ),
          mIsHighscore( 0 ),
          mHomeButton( mainFont, 4, -4, translate( "returnHome" ) ),
          mReturnToHome( false ) {

    addComponent( &mHomeButton );
    mHomeButton.addActionListener( this );

    mHomeButton.setMouseOverTip( "" );

    }


        
CompetitionCheckinPage::~CompetitionCheckinPage() {
    if( mWebRequest != -1 ) {
        clearWebRequestSerial( mWebRequest );
        }
    if( mPriceList != NULL ) {
        delete [] mPriceList;
        }
    if( mMoveList != NULL ) {
        delete [] mMoveList;
        }

    }



char CompetitionCheckinPage::getReturnToHome() {
    return mReturnToHome;
    }




void CompetitionCheckinPage::setPriceList( char *inPriceList ) {
    if( mPriceList != NULL ) {
        delete [] mPriceList;
        }
    mPriceList = stringDuplicate( inPriceList );
    }

void CompetitionCheckinPage::setMoveList( char *inMoveList ) {
    if( mMoveList != NULL ) {
        delete [] mMoveList;
        }
    mMoveList = stringDuplicate( inMoveList );
    }


void CompetitionCheckinPage::setSuccess( int inSuccess ) {
    mSuccess = inSuccess;
    }

void CompetitionCheckinPage::setCompetitionID( int inCompetitionID ) {
    mCompetitionID = inCompetitionID;
    }



void CompetitionCheckinPage::actionPerformed( GUIComponent *inTarget ) {
    if( inTarget == &mHomeButton ) {
        mReturnToHome = true;
        }
    }


void CompetitionCheckinPage::step() {
    if( mWebRequest != -1 ) {
            
        int stepResult = stepWebRequestSerial( mWebRequest );
        
        if( stepResult != 0 ) {
            setWaiting( false );
            }

        switch( stepResult ) {
            case 0:
                break;
            case -1:
                mStatusError = true;
                mStatusMessageKey = "err_webRequest";
                clearWebRequestSerial( mWebRequest );
                mWebRequest = -1;
                mHomeButton.setVisible( true );
                break;
            case 1: {
                char *result = getWebResultSerial( mWebRequest );
                clearWebRequestSerial( mWebRequest );
                mWebRequest = -1;
                     
                printf( "Web result = %s\n", result );
   
                if( strstr( result, "DENIED" ) != NULL ) {
                    mStatusError = true;
                    mStatusMessageKey = "houseCheckInFailed";
                    mHomeButton.setVisible( true );
                    }
                else {
                    // house checked in!
                    
                    SimpleVector<char *> *tokens =
                        tokenizeString( result );
                    
                    if( tokens->size() != 2 ||
                        strcmp( *( tokens->getElement( 1 ) ), "OK" ) != 0 ) {
                        mStatusError = true;
                        mStatusMessageKey = "err_badServerResponse";
                        mHomeButton.setVisible( true );   
                        }
                    else {
                        int isHighscore;
                        sscanf( *( tokens->getElement( 0 ) ), 
                                "%d", &isHighscore );
                        
                        if( isHighscore )
                            mStatusMessageKey = "newHighscore";
                        else
                            mStatusMessageKey = "notNewHighscore";
                        mHomeButton.setVisible( true );
                        }

                    for( int i=0; i<tokens->size(); i++ ) {
                        delete [] *( tokens->getElement( i ) );
                        }
                    delete tokens;
                    }
                        
                        
                delete [] result;
                }
                break;
            }
        }
    }



        
void CompetitionCheckinPage::makeActive( char inFresh ) {
    if( !inFresh ) {
        return;
        }
    
    // send back to server            
    char *ticketHash = getTicketHash();    

    char *actionString = autoSprintf( 
        "action=end_competition&user_id=%d"
        "&%s"
        "&competition_id=%d"
        "&success=%d"
        "&price_list=%s"
        "&move_list=%s",
        userID, ticketHash, 
        mCompetitionID, mSuccess, mPriceList, mMoveList );
    delete [] ticketHash;
 
    mWebRequest = startWebRequestSerial( "POST", 
                                   serverURL, 
                                   actionString );

    delete [] actionString;

    mReturnToHome = false;
    
    mStatusError = false;
    mStatusMessageKey = NULL;

    setStatusDirect( NULL, false );
    

    mHomeButton.setVisible( false );

    setWaiting( true );
    }
